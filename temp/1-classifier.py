import random
import math
 
infilename = "allTraining.txt"
trainingdata = open(infilename).read()

TotalWords = 0	#Total number of words
POSTotals = {}	#Total number of each Part of Speech
emitCounts = {}	#Total number of times each word occurs for each Part of Speech
vocabulary = set()

contextconst = ""
contexts = {}	#List of which POS's occur after each other one. 
context = contextconst

states = []

class Step:
	prob = {}
	prev = {}
 
for word_part in trainingdata.split():
	word_part = word_part.split("_")
	word = word_part[0].lower()
	POS = word_part[1]
	if not POS in emitCounts:
		POSTotals[POS] = 0
		states.append(POS)
		emitCounts[POS] = {}
	if not word in emitCounts[POS]:
		emitCounts[POS][word] = 1
	else:
		emitCounts[POS][word] += 1
	POSTotals[POS] += 1
	if not context in contexts:
		contexts[context] = {}
	if not POS in contexts[context]:
		contexts[context][POS] = 1
	else:
		contexts[context][POS] += 1
	context = POS
	TotalWords += 1
	vocabulary.add(word)

#Get contexts[POS1][POS2]/POSTotals[POS1] to get transition prob from 1 to 2
#Get emitCounts[POS][word]/POSTotals[POS] to get the emission prob of a word given the POS

#NOW DO THE TEST DATA
infilename = "devtest.txt"
testdata = open(infilename).read()

#--== Read Test data ==--#
truePath = []
obs = []
for word_part in testdata.split():
	word_part = word_part.split("_")
	word = word_part[0].lower()
	POS = word_part[1]
	obs.append(word)
	truePath.append(POS)	#Keep track of the actual values for use in the confusion matrix later

#--== Calculate log probabiliites ==--#
	#---=== Start Probability for each context ===---#
start_p = {}
for s in states:
	start_p[s] = math.log(float(POSTotals[s])/float(TotalWords))

	#---=== Transition Probabilities ===---#
trans_p = {}
for s in states:
	trans_p[s] = {}
	for x in states:
		if not x in contexts[s]:
			trans_p[s][x] = 1.0/float(POSTotals[s] + 1)
		else:
			trans_p[s][x] = float(contexts[s][x] + 1) /float(POSTotals[s] + 1)
		trans_p[s][x] = math.log(trans_p[s][x])

	#---=== Emission Probabilities ===---#
emit_p = {}
for s in states:
	emit_p[s] = {}
	for word in emitCounts[s]:
		emit_p[s][word] = math.log(float(emitCounts[s][word] + 1) /float(POSTotals[s] + 1))

def viterbi(obs, states, start_p, trans_p, emit_p):
	V = [{}]
	path = {}
 
	# Initialize base cases (t == 0)
	for y in states:
		if not obs[0] in emit_p[y]:
			V[0][y] = start_p[y] + math.log(1.0/float(POSTotals[y] + 1))
		else:
		  	V[0][y] = start_p[y] + emit_p[y][obs[0]]
		path[y] = [y]

	# Run Viterbi for t > 0
	noEmit = math.log(1.0/float(TotalWords)) #If a word has 0 emit probability, smooth this from 0 to 1 / total number of training words
	for t in range(1, len(obs)):
		#print t
		V.append({})
		newpath = {}
 
		for y in states:
			(prob, state) = max((V[t-1][y0] + trans_p[y0][y] + emit_p[y].get(obs[t], noEmit), y0) for y0 in states)
			V[t][y] = prob					#Store the new State probability
			newpath[y] = path[state] + [y]	#Store the new path for this state
 
		# Don't need to remember the old paths
		path = newpath
	n = 0		   # if only one element is observed max is sought in the initialization values
	if len(obs)!=1:
		n = t
	#print_dptable(V)
	(prob, state) = max((V[n][y], y) for y in states)
	print state
	return (prob, path[state])

(prob, path) = viterbi(obs, states, start_p, trans_p, emit_p)


#--== Compare results against True values ==--#
correct_predictions = 0
confusion = {}
for POS in POSTotals:
	confusion[POS] = {}
	for POSinner in POSTotals:
		confusion[POS][POSinner] = 0

for i in range(0, len(truePath) - 1):
	if path[i] == truePath[i]:
		correct_predictions += 1
	confusion[truePath[i]][path[i]] += 1

print "Accuracy: %f" % (float(correct_predictions) / float(len(truePath)))
print "Confusion Matrix"
print confusion.keys()
for POS in confusion:
	print confusion[POS].values()
