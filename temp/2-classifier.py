import random
import math
 
infilename = "allTraining.txt"
trainingdata = open(infilename).read()

TotalWords = 0	#Total number of words
POSTotals = {}	#Total number of each Part of Speech
emitCounts = {}	#Total number of times each word occurs for each Part of Speech
vocabulary = set()

contextconst = ""
context1 = contextconst
context2 = contextconst
context = context1 + " " + context2
contexts = {}	#List of which POS's occur after each other one. 
TotalContexts = 0
ContextTotals = {}

states = []

class Step:
	prob = {}
	prev = {}
 
for word_part in trainingdata.split():
	word_part = word_part.split("_")
	word = word_part[0].lower()
	POS = word_part[1]
	if not POS in emitCounts:
		POSTotals[POS] = 0
		states.append(POS)
		emitCounts[POS] = {}
	if not word in emitCounts[POS]:
		emitCounts[POS][word] = 1
	else:
		emitCounts[POS][word] += 1
	POSTotals[POS] += 1
	TotalWords += 1
	vocabulary.add(word)

#Make initial dictionary of contexts 

for word_part in trainingdata.split():
	POS = word_part.split("_")[1]
	if not context in contexts:
		contexts[context] = {}
		ContextTotals[context] = 0
	if not POS in contexts[context]:
		contexts[context][POS] = 1
	else:
		contexts[context][POS] += 1
	ContextTotals[context] += 1
	context1 = context2
	context2 = POS
	context = context1 + " " + context2

TotalContexts = TotalWords - 2

#Get contexts[POS1][POS2]/POSTotals[POS1] to get transition prob from 1 to 2
#Get emitCounts[POS][word]/POSTotals[POS] to get the emission prob of a word given the POS

#NOW DO THE TEST DATA
infilename = "devtesthalf.txt"
testdata = open(infilename).read()

#--== Read Test data ==--#
truePath = []
obs = []
for word_part in testdata.split():
	word_part = word_part.split("_")
	word = word_part[0].lower()
	POS = word_part[1]
	obs.append(word)
	truePath.append(POS)		#Keep track of the actual values for use in the confusion matrix later

#--== Calculate log probabiliites ==--#
	#---=== Start Probability for each context ===---#
start_p = {}
for s1 in states:
	for s2 in states:
		if not (s1 + " " + s2) in contexts:
			start_p[s1 + " " + s2] = math.log(1.0/float(TotalContexts))
		else:
			start_p[s1 + " " + s2] = math.log(float(ContextTotals[s1 + " " + s2])/float(TotalContexts))

	#---=== Transition Probabilities ===---#
trans_p = {}
for s1 in states:
	for s2 in states:
		context = s1 + " " + s2
		trans_p[context] = {}
		if not context in contexts:	#If this context has not been seen, transition to each state is equal probability
			for x in states:
				trans_p[context][x] = math.log(1.0/float(len(states)))
		else:
			for x in states:
				if not x in contexts[context]:
					trans_p[context][x] = math.log(1.0/float(ContextTotals[context] + 1))
				else:
					trans_p[context][x] = math.log(float(contexts[context][x] + 1) /float(ContextTotals[context] + 1))

	#---=== Emission Probabilities ===---#
emit_p = {}
for s in states:
	emit_p[s] = {}
	for word in emitCounts[s]:
		emit_p[s][word] = math.log(float(emitCounts[s][word] + 1) /float(POSTotals[s] + 1))

def viterbi(obs, states, start_p, trans_p, emit_p):
	V = {}
 
	# Initialize base cases (t == 0)
	for state1 in states:
		for state2 in states:
			V[state1 + " " + state2] = ([state2], start_p[state1 + " " + state2])
	counter = 0

	noEmit = math.log(1.0/float(TotalWords)) #If a word has 0 emit probability, smooth this from 0 to 1 / total number of training words
	for o in obs:
		U = {}
		for next_state in states:
			argmax = None
			valmax = float("-inf")
			for curr_state in states:
				for prev_state in states:
					(v_path, v_prob) = V[prev_state + " " + curr_state]
					p = trans_p[prev_state + " " + curr_state][next_state] + emit_p[curr_state].get(o, noEmit)	#If 0 emit prob for a word, smooth by using a very small values instead (noEmit)
					v_prob += p
					if v_prob > valmax:
						argmax = v_path + [next_state]
						valmax = v_prob
				U[curr_state + " " + next_state] = (argmax, valmax)
		V = U
		counter += 1
		print counter
	
	#Find winning end state
	argmax = None
	valmax = float("-inf")
	for s1 in states:
		for s2 in states:
			if not (s1 + " " + s2) in V:
				(v_path, v_prob) = V[s1 + " " + s2] = (None, float("-inf"))
			else:
				(v_path, v_prob) = V[s1 + " " + s2]
			if v_prob > valmax:
				argmax = v_path
				valmax = v_prob
	return (argmax, valmax)

(path, prob) = viterbi(obs, states, start_p, trans_p, emit_p)

#--== Compare results against True values ==--#
correct_predictions = 0
confusion = {}
for POS in POSTotals:
	confusion[POS] = {}
	for POSinner in POSTotals:
		confusion[POS][POSinner] = 0

for i in range(0, len(truePath) - 1):
	if path[i] == truePath[i]:
		correct_predictions += 1
	confusion[truePath[i]][path[i]] += 1

print "Accuracy: %f" % (float(correct_predictions) / float(len(truePath)))
print "Confusion Matrix"
print confusion.keys()
for POS in confusion:
	print confusion[POS].values()
