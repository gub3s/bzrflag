import random
 
infilename = "bom.txt"
trainingdata = open(infilename).read()

contexts = {}	#List of the number of times each word occurred after each other word
wordTotals = {}	#Total number of words following each word
 
contextconst = ""
 
context1 = contextconst	#The first word in a sequence
context2 = contextconst	#The second word in a sequence
context = context1 + " " + context2
model = {}
 
for word in trainingdata.split():
	if not context in contexts:
		contexts[context] = {}
		wordTotals[context] = 0
	if not word in contexts[context]:
		contexts[context][word] = 1
	else:
		contexts[context][word] += 1
	wordTotals[context] += 1
	context1 = context2
	context2 = word
	context = context1 + " " + context2

for word in contexts['The Lord']:
	print word + "\t" + str(float(contexts['The Lord'][word])/float(wordTotals['The Lord']))

print len(contexts)
context1 = contextconst
context2 = contextconst
context = context1 + " " + context2
s = ""
for i in range(1000):
	wordChoice = random.randint(1, wordTotals[context])
	for word in contexts[context]:
		wordChoice -= contexts[context][word]
		if wordChoice <= 0:
			s += word + " "
			context1 = context2
			context2 = word
			context = context1 + " " + context2
			break
 
print(s)
