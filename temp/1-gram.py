import random
 
infilename = "bom.txt"
trainingdata = open(infilename).read()

contexts = {}	#List of the number of times each word occurred after each other word
wordTotals = {}	#Total number of words following each word
 
contextconst = ""
 
context = contextconst
model = {}
 
for word in trainingdata.split():
	if not context in contexts:
		contexts[context] = {}
		wordTotals[context] = 0
	if not word in contexts[context]:
		contexts[context][word] = 1
	else:
		contexts[context][word] += 1
	wordTotals[context] += 1
	context = word

for word in contexts['The']:
	print word + "\t" + str(float(contexts['The'][word])/float(wordTotals['The']))

print len(contexts)
context = contextconst
s = ""
for i in range(100):
	wordChoice = random.randint(1, wordTotals[context])
	for word in contexts[context]:
		wordChoice -= contexts[context][word]
		if wordChoice <= 0:
			s += word + " "
			context = word
			break
 
print(s)
