from os import listdir
import math

short_list = "naivebayes/ShortStop.txt"
long_list = "naivebayes/LongStop.txt"

training_dir = "naivebayes/20news-bydate/20news-bydate-train"
testing_dir = "naivebayes/20news-bydate/20news-bydate-test"

stopwords = set()
probabilities = {}

# Read file to set
def readFileToSet(filename):
	f_set = set()
	with open(filename) as f:
		f_set = set(line.strip() for line in f)
	#print stopwords, len(stopwords)
	return f_set

	
def cleanWord(word):
	word = word.strip()
	word = word.translate(None,".,?!/><;:\"()[]{}\t").lower()	
	return word

def main():
	# create stopwords set
	stopwords = readFileToSet(long_list)
	#stopwords_file = open(short_list)
	#stopwords = set(line.strip() for line in stopwords_file)
	#print stopwords, len(stopwords)

	# get news groups list
	news_groups = listdir(training_dir)
	#print news_groups, len(news_groups)

	# read files for each news group
	news_dict = {}
	group_totals = {}
	all_words = set()
	total_files = 0
	group_files = {}
	for group in news_groups:
		path = training_dir + "/" + group
		files = listdir(path)
		group_files[group] = len(files)
		total_files += len(files)
		#print files, len(files)
		
		group_words = {}
		group_total = 0;
		for file_name in files:
			file_path = path + "/" + file_name
			current_file = open(file_path)
			for line in current_file.readlines():
				words = line.split(" ")
				for word in words:
					word = cleanWord(word)
					if word in stopwords:
						continue
					all_words.add(word)
					group_total += 1
					if not word in group_words:
						group_words[word] = 1
					else:
						group_words[word] += 1
		news_dict[group] = group_words
		group_totals[group] = group_total
		
	biggest_group = ""
	biggest_file_count = float("-inf")	
	for group in news_groups:
		file_count = group_files[group]
		temp = max(biggest_file_count, file_count)
		if temp > biggest_file_count:
			biggest_group = group
			biggest_file_count = temp
	print float(biggest_file_count)/float(total_files)
	
	path = testing_dir + "/" + biggest_group
	target_group_files = listdir(path)
	target_file_count = len(target_group_files)
	
	
	#  #print news_dict, len(news_dict)
	#  #print all_words, len(all_words)
	#  
	# for word in all_words:
	# 	word_probability = {}
	# 	for group in news_groups:
	# 		 group_words = news_dict[group]
	# 		 if not word in group_words:
	# 			word_probability[group] = 1.0/float(group_totals[group]+1)
	# 		 else:
	# 			word_probability[group] = float(group_words[word]+1)/float(group_totals[group]+1)
	# 	probabilities[word] = word_probability
	# #print probabilities, len(probabilities)
	# 
	# group_probability = {}
	# for group in news_groups:
	# 	group_probability[group] = float(group_files[group])/float(total_files)
	#  #print group_probability, len(group_probability)
	# 
	test_groups = listdir(testing_dir)
	# 
	# correct_predictions = 0
	total_files = 0
	# actual_categories = {}
	for group in test_groups:
	# 	predicted_category = {}
		# for inner_group in test_groups:
	# 		predicted_category[inner_group] = 0
		inner_path = testing_dir + "/" + group
		files = listdir(inner_path)
		total_files += len(files)
	# 	for file_name in files:
	# 		file_path = path + "/" + file_name
	# 		current_file = open(file_path)
	# 		scores = {}
	# 		for line in current_file.readlines():
	# 			words = line.split(" ")
	# 			for word in words:
	# 				word  = cleanWord(word)
	# 				if word in stopwords:
	# 					#print "STOPWORD"
	# 					continue
	# 				word_probs = {}
	# 				if not word in probabilities:
	# 					#print "NO PROBABILITY"
	# 					continue
	# 				word_probs = probabilities[word]
	# 				for inner_group in word_probs:
	# 					if not inner_group in scores:
	# 						scores[inner_group] = math.log(group_probability[inner_group])
	# 						scores[inner_group] += math.log(word_probs[inner_group])
	# 					else:
	# 					 	scores[inner_group] += math.log(word_probs[inner_group])
	# 		best_group = ""
	# 		best_score = float("-inf")
	# 		for inner_group in scores:
	# 			temp_score = max(best_score, scores[inner_group])
	# 			if temp_score > best_score:
	# 				best_score = temp_score
	# 				best_group = inner_group
	# 		predicted_category[best_group] += 1
	# 	# print group, predicted_category	
	# 	# print
	# 	# print float(predicted_category[group])/float(len(files))
	# 	correct_predictions += predicted_category[group]
	# 	total_files += len(files)
	# 	actual_categories[group] = predicted_category
	# # print actual_categories
	# print float(correct_predictions)/float(total_files)
	# 
	# for group in test_groups:
	# 	print group
	# 	print actual_categories[group].values()
	
	print float(target_file_count)/float(total_files)

if __name__ == '__main__':
	 main()

