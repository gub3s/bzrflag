from os import listdir
import math

short_list = "naivebayes/ShortStop.txt"
long_list = "naivebayes/LongStop.txt"

training_dir = "naivebayes/20news-bydate/20news-bydate-train"
testing_dir = "naivebayes/20news-bydate/20news-bydate-test"

stopwords = set()
probabilities = {}

# Read file to set
def readFileToSet(filename):
	f_set = set()
	with open(filename) as f:
		f_set = set(line.strip() for line in f)
	#print stopwords, len(stopwords)
	return f_set

	
def cleanWord(word):
	word = word.strip()
	word = word.translate(None,".,?!/><;:\"()[]{}\t").lower()	
	return word

def main():
	# create stopwords set
	stopwords = readFileToSet(short_list)
	#stopwords_file = open(short_list)
	#stopwords = set(line.strip() for line in stopwords_file)
	#print stopwords, len(stopwords)

	# get news groups list
	news_groups = listdir(training_dir)
	#print news_groups, len(news_groups)

	# read files for each news group
	news_dict = {}
	group_totals = {}
	all_words = set()
	total_files = 0
	group_files = {}
	for group in news_groups:
		path = training_dir + "/" + group
		files = listdir(path)
		group_files[group] = len(files)
		total_files += len(files)
		#print files, len(files)
		
		for file_name in files:
			file_path = path + "/" + file_name
			current_file = open(file_path)
			for line in current_file.readlines():
				words = line.split(" ")
				for word in words:
					word = cleanWord(word)
					if word in stopwords:
						continue
					all_words.add(word)
	
	 #print news_dict, len(news_dict)
	 #print all_words, len(all_words)

	word_occurances = {}
	for word in all_words:
		word_occurances[word] = {}
	
	for group in news_groups:
		path = training_dir + "/" + group
		files = listdir(path)
		
		for file_name in files:
			document_words = set()
			file_path = path + "/" + file_name
			current_file = open(file_path)
			for line in current_file.readlines():
				words = line.split(" ")
				for word in words:
					word = cleanWord(word)
					if word in stopwords:
						continue
					document_words.add(word)
			for word in document_words:
				if not group in word_occurances[word]:
					word_occurances[word][group] = 1
				else:
					word_occurances[word][group] += 1
	# print word_occurances		
	 
	for word in all_words:
	 	word_probability = {}
	 	for group in news_groups:
	 		 if not group in word_occurances[word]:
	 			word_probability[group] = 1.0/float(group_files[group]+2)
	 		 else:
	 			word_probability[group] = float(word_occurances[word][group]+1)/float(group_files[group]+2)
	 	probabilities[word] = word_probability
	# print probabilities, len(probabilities)

	group_probability = {}
	for group in news_groups:
		group_probability[group] = float(group_files[group])/float(total_files)
	 #print group_probability, len(group_probability)
	
		
	test_groups = listdir(testing_dir)
	correct_predictions = 0
	total_files = 0
	actual_categories = {}
	base_scores = {}
	for group in test_groups:
		group_score = 0
		for word in all_words:
			word_probs = probabilities[word]
			group_score += math.log(1 - word_probs[group])
		base_scores[group] = group_score
	for group in test_groups:
		predicted_category = {}
		for inner_group in test_groups:
			predicted_category[inner_group] = 0
		path = testing_dir + "/" + group
		files = listdir(path)
		for file_name in files:
			file_path = path + "/" + file_name
			current_file = open(file_path)
			scores = {}
			
			document_words = set()
			file_path = path + "/" + file_name
			current_file = open(file_path)
			for line in current_file.readlines():
				words = line.split(" ")
				for word in words:
					word = cleanWord(word)
					if word in stopwords:
						continue
					document_words.add(word)
			
			for word in document_words:
				if not word in all_words:
					continue
				else:
					word_probs = probabilities[word]
					for inner_group in word_probs:
						if not inner_group in scores:
							scores[inner_group] = math.log(group_probability[inner_group]) + base_scores[inner_group]
							scores[inner_group] += math.log(word_probs[inner_group]) - math.log(1 - word_probs[inner_group])
						else:
							scores[inner_group] += math.log(word_probs[inner_group]) - math.log(1 - word_probs[inner_group])
			best_group = ""
			best_score = float("-inf")
			for inner_group in scores:
				temp_score = max(best_score, scores[inner_group])
				if temp_score > best_score:
					best_score = temp_score
					best_group = inner_group
			predicted_category[best_group] += 1
		# print group, predicted_category	
		# print
		print group, float(predicted_category[group])/float(len(files))
		correct_predictions += predicted_category[group]
		total_files += len(files)
		actual_categories[group] = predicted_category
	# print actual_categories
	print "Total Accuracy:", float(correct_predictions)/float(total_files)
		 
	for group in test_groups:
		print group
		print actual_categories[group].values()

if __name__ == '__main__':
	 main()

