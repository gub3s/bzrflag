#!/usr/bin/env python
'''This is a demo on how to use Gnuplot for potential fields.  We've
intentionally avoided "giving it all away."
'''

from __future__ import division
from itertools import cycle

import sys
import math
import time
import random

from bzrc import BZRC, Command
from random import randint



# Process CLI arguments.
try:
	execname, host, port = sys.argv
except ValueError:
	execname = sys.argv[0]
	print >>sys.stderr, '%s: incorrect number of arguments' % execname
	print >>sys.stderr, 'usage: %s hostname port' % sys.argv[0]
	sys.exit(-1)

# Connect.
#bzrc = BZRC(host, int(port), debug=True)
bzrc = BZRC(host, int(port))
constants = bzrc.get_constants()

try:
	from numpy import linspace

except ImportError:
	# This is stolen from numpy.  If numpy is installed, you don't
	# need this:
	def linspace(start, stop, num=50, endpoint=True, retstep=False):
		"""Return evenly spaced numbers.

		Return num evenly spaced samples from start to stop.  If
		endpoint is True, the last sample is stop. If retstep is
		True then return the step value used.
		"""
		num = int(num)
		if num <= 0:
			return []
		if endpoint:
			if num == 1:
				return [float(start)]
			step = (stop-start)/float((num-1))
			y = [x * step + start for x in xrange(0, num - 1)]
			y.append(stop)
		else:
			step = (stop-start)/float(num)
			y = [x * step + start for x in xrange(0, num)]
		if retstep:
			return y, step
		else:
			return y


########################################################################
# Constants

# Output file:
FILENAME = 'fields.gpi'
# Size of the world (one of the "constants" in bzflag):
WORLDSIZE = 800
FIELDSIZE = 200
# How many samples to take along each dimension:
SAMPLES = 50
# Change spacing by changing the relative length of the vectors.  It looks
# like scaling by 0.75 is pretty good, but this is adjustable:
VEC_LEN = 0.75 * WORLDSIZE / SAMPLES
# Animation parameters:
ANIMATION_MIN = 0
ANIMATION_MAX = 500
ANIMATION_FRAMES = 50


########################################################################
# Field and Obstacle Definitions

def generate_field_function(scale):
	def function(x, y):
		'''User-defined field function.'''
		sqnorm = (x**2 + y**2)
		if sqnorm == 0.0:
			return 0, 0
		else:
			return x*scale/sqnorm, y*scale/sqnorm
	return function

OBSTACLES = bzrc.get_obstacles()

########################################################################
# Helper Functions

def gpi_point(x, y, vec_x, vec_y):
	'''Create the centered gpi data point (4-tuple) for a position and
	vector.  The vectors are expected to be less than 1 in magnitude,
	and larger values will be scaled down.'''
	r = (vec_x ** 2 + vec_y ** 2) ** 0.5
	if r > 1:
		vec_x /= r
		vec_y /= r
	return (x + vec_x * VEC_LEN / 2, y + vec_y * VEC_LEN / 2,
			-vec_x * VEC_LEN, -vec_y * VEC_LEN)

def gnuplot_header(minimum, maximum):
	'''Return a string that has all of the gnuplot sets and unsets.'''
	s = ''
	s += 'set xrange [%s: %s]\n' % (minimum, maximum)
	s += 'set yrange [%s: %s]\n' % (minimum, maximum)
	# The key is just clutter.  Get rid of it:
	s += 'unset key\n'
	# Make sure the figure is square since the world is square:
	s += 'set size square\n'
	# Add a pretty title (optional):
	s += "set title 'Combined Fields'\n"
	return s

def draw_line(p1, p2):
	'''Return a string to tell Gnuplot to draw a line from point p1 to
	point p2 in the form of a set command.'''
	x1, y1 = p1
	x2, y2 = p2
	return 'set arrow from %s, %s to %s, %s nohead lt 3\n' % (x1, y1, x2, y2)

def draw_obstacles(obstacles):
	'''Return a string which tells Gnuplot to draw all of the obstacles.'''
	s = 'unset arrow\n'

	for obs in obstacles:
		last_point = obs[0]
		for cur_point in obs[1:]:
			s += draw_line(last_point, cur_point)
			last_point = cur_point
		s += draw_line(last_point, obs[0])
	return s

def plot_field(function):
	'''Return a Gnuplot command to plot a field.'''
	s = "plot '-' with vectors head\n"

	separation = WORLDSIZE / SAMPLES
	end = WORLDSIZE / 2 - separation / 2
	start = -end * 2

	points = ((x, y) for x in linspace(start, end, SAMPLES)
				for y in linspace(start, end, SAMPLES))
			
#---=== Start plotting points ===---#
	mytanks, othertanks, flags, shots = bzrc.get_lots_o_stuff()
	enemies = [tank for tank in othertanks if tank.color !=
					constants['team']]
		
	print "checking tanks"
	for x, y in points:

		flag_rad = 100
		enemy_rad = 100
		base_rad = 100
		obstacle_rad = 50
		closest_dist = 12000
		Xdir = 0
		Ydir = 0
		target_flag = ''

	#---=== IF YOU HAVE NO FLAG, GO GET ONE ===---#
		for flag in flags:
			distanceX = flag.x - x
			distanceY = flag.y - y
			tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)
			if tot_distance < closest_dist:
				closest_dist = tot_distance
				target_flag = flag.color

		for flag in flags:
			if flag.color != target_flag:
				continue
			distanceX = flag.x - x
			distanceY = flag.y - y
			tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)

			if tot_distance > flag_rad: #If you are very far from flag or aimed straight, go full speed
				Xdir += distanceX / tot_distance
				Ydir += distanceY / tot_distance

			elif tot_distance > 0:   #Otherwise, slow down the closer you get
				Xdir += (distanceX / tot_distance) * (tot_distance / flag_rad)
				Ydir += (distanceY / tot_distance) * (tot_distance / flag_rad)

			else:   #If you are very very close, this is the minimum speed
				Xdir += (distanceX / tot_distance) * (75.0 / flag_rad)
				Ydir += (distanceY / tot_distance) * (75.0 / flag_rad)

	#---=== Get repulsed from every enemy tank ===---#
		for enemy in enemies:				
			distanceX = enemy.x - x
			distanceY = enemy.y - y
			tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)

			if tot_distance > enemy_rad:	#If you are too far away, don't worry
				continue

			Xdir -= (distanceX / tot_distance) * (enemy_rad - tot_distance) / enemy_rad
			Ydir -= (distanceY / tot_distance) * (enemy_rad - tot_distance) / enemy_rad
			#print "xdir: %s" % Xdir

	#---=== Get pushed tangentially along obstacles ===---#
		#Xdir = 0		
		#Ydir = 0
		
		for obstacle in OBSTACLES: 
			#Get two closest points on the obstacle
			centerX = 0
			centerY = 0
			closest1 = -1
			closest2 = -1
			radius = 0

			for point in obstacle:
				centerX += point[0]
				centerY += point[1]
			centerX /= 4
			centerY /= 4
			radius = 25 + math.sqrt((obstacle[0][0] - centerX) ** 2 + (obstacle[0][1] - centerY) ** 2)
			#print radius
			distanceX = centerX - x
			distanceY = centerY - y
			target_angle = math.atan2(distanceY, distanceX)
			tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)
	
			if tot_distance > radius: #Ignore if you are too far away
				continue

			#print "edgeX: %s" % edgeX
			#Xdir = 0.0 #Lessen the effect of the flag PField at this point
			#Ydir = 0.0
			Xdir += math.cos(target_angle + math.pi / 2)
			Ydir += math.sin(target_angle + math.pi / 2)


		plotvalues = gpi_point(x, y, -Xdir, -Ydir)
		if plotvalues is not None:
			x1, y1, x2, y2 = plotvalues
			s += '%s %s %s %s\n' % (x1, y1, x2, y2)
	s += 'e\n'
	return s

########################################################################
# Plot the potential fields to a file
print 'making file'
outfile = open(FILENAME, 'w')
print >>outfile, gnuplot_header(-WORLDSIZE / 2, WORLDSIZE / 2)
print >>outfile, draw_obstacles(OBSTACLES)
field_function = generate_field_function(150)
print >>outfile, plot_field(field_function)


########################################################################
# Animate a changing field, if the Python Gnuplot library is present

try:
	from Gnuplot import GnuplotProcess
except ImportError:
	print "Sorry.  You don't have the Gnuplot module installed."
	import sys
	sys.exit(-1)

forward_list = list(linspace(ANIMATION_MIN, ANIMATION_MAX, ANIMATION_FRAMES/2))
backward_list = list(linspace(ANIMATION_MAX, ANIMATION_MIN, ANIMATION_FRAMES/2))
anim_points = forward_list + backward_list

gp = GnuplotProcess(persist=False)
gp.write(gnuplot_header(-WORLDSIZE / 2, WORLDSIZE / 2))
gp.write(draw_obstacles(OBSTACLES))
for scale in cycle(anim_points):
	field_function = generate_field_function(scale)
	gp.write(plot_field(field_function))

# vim: et sw=4 sts=4
