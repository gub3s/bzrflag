#!/usr/bin/python -tt

# An incredibly simple agent.  All we do is find the closest enemy tank, drive
# towards it, and shoot.  Note that if friendly fire is allowed, you will very
# often kill your own tanks with this code.

#################################################################
# NOTE TO STUDENTS
# This is a starting point for you.  You will need to greatly
# modify this code if you want to do anything useful.  But this
# should help you to know how to interact with BZRC in order to
# get the information you need.
#
# After starting the bzrflag server, this is one way to start
# this code:
# python agent0.py [hostname] [port]
#
# Often this translates to something like the following (with the
# port name being printed out by the bzrflag server):
# python agent0.py localhost 49857
#################################################################

from __future__ import division
import sys
import math
import time
import random
import numpy


from bzrc import BZRC, Command
from random import randint
from itertools import cycle

class Agent(object):
	"""Class handles all command and control logic for a teams tanks."""

	#10_DEGREES = 0.1745

	def __init__(self, bzrc):
		self.bzrc = bzrc
		self.constants = self.bzrc.get_constants()
		self.commands = []
		self.movement_timer = 0
		self.target_turn_time = 0
		self.needs_target_turn_time = True
		self.target_shoot_time = 0
		self.needs_target_shoot_time = True
		self.shoot_timer = 0
		self.bulletSpeed = int(self.constants['shotspeed'])
		self.target = numpy.matrix([[0],[0]])
		bases = self.bzrc.get_bases()
		for base in bases:
			if base.color == self.constants['team']:
				self.mybase = base
				break

		print self.constants	
		self.myBaseX = 0.5 * (self.mybase.corner1_x + self.mybase.corner3_x)
		self.myBaseY = 0.5 * (self.mybase.corner1_y + self.mybase.corner3_y)

		print "defining consts"
		
		mytanks, othertanks, flags, shots = self.bzrc.get_lots_o_stuff()
		self.enemies = [tank for tank in othertanks if tank.color !=
				self.constants['team']]

		global dt, F, Sx, H, Sz, mu, S
		dt = 0.5
		mu = {}
		S = {}
		
		for enemy in self.enemies:
			print enemy.callsign
			#---=== Constant Matrices; Calculate once ===---#

			F = numpy.matrix([[1, dt, dt*dt/2, 0, 0, 0],	#Physics equations
							  [0, 1, dt, 0, 0, 0],
							  [0, 0, 1, 0, 0, 0],
							  [0, 0, 0, 1, dt, dt*dt/2],
							  [0, 0, 0, 0, 1, dt],
							  [0, 0, 0, 0, 0, 1]])

			Sx = numpy.matrix([[0.1, 0, 0, 0, 0, 0],  #Uncertainty in physics
							   [0, 0.1, 0, 0, 0, 0],
							   [0, 0, 1, 0, 0, 0],
							   [0, 0, 0, 0.1, 0, 0],
							   [0, 0, 0, 0, 0.1, 0],
							   [0, 0, 0, 0, 0, 10]])				

			H = numpy.matrix([[1, 0, 0, 0, 0, 0],	#Pick out X and Y
							  [0, 0, 0, 1, 0, 0]])

			Sz = numpy.matrix([[9, 0],	 #Uncertainty in X, Y sensor
							   [0, 9]])
				#---=== =============================== === ---#

				#---=== To be updated each tick; But initialize some guesses ===---#
			mu[enemy.callsign] = numpy.matrix([[0], #x	  #Estimate of tank state
								[0], #x'
								[0], #x''
								[0], #y
								[0], #y'
								[0]])#y''

			S[enemy.callsign] = numpy.matrix([[100, 0, 0, 0, 0, 0], #Uncertainty in estimate
							  [0, 0.1, 0, 0, 0, 0],
							  [0, 0, 0.1, 0, 0, 0],
							  [0, 0, 0, 100, 0, 0],
							  [0, 0, 0, 0, 0.1, 0],
							  [0, 0, 0, 0, 0, 0.1]])
				#---=== =============================== === ---#

	def updateKalman(self):
		global F, S, mu, Sx, Sz, H, dt
		for enemy in self.enemies:
			if(enemy.status != 'alive'):  #If enemy is dead, skip calculations
				continue

			distanceX = self.myBaseX - mu[enemy.callsign][0]
			distanceY = self.myBaseY - mu[enemy.callsign][3]
			tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)
			if tot_distance > 10 * self.constants['shotrange']:
				continue

			#Observation of enemy state
			Z = numpy.matrix([[enemy.x], #x	  
							[enemy.y]]) #y			
		
		# ---=== UPDATE MATRICES ===--- #						
			FSFTSX = F * S[enemy.callsign] * F.T + Sx
			K = (FSFTSX)*(H.T)*(H*(FSFTSX)*H.T + Sz).I		
			mu[enemy.callsign] = F*mu[enemy.callsign] + K * (Z - H*F*mu[enemy.callsign])
			S[enemy.callsign] = (numpy.identity(6) - K*H)*(FSFTSX)

	def moveToPost(self, tank):
		#Get distance to post
		if tank.index == 0:
			X = self.mybase.corner1_x
			Y = self.mybase.corner1_y
		elif tank.index == 1:
			X = self.mybase.corner2_x
			Y = self.mybase.corner2_y
		elif tank.index == 2:
			X = self.mybase.corner3_x
			Y = self.mybase.corner3_y
		elif tank.index == 3:
			X = self.mybase.corner4_x
			Y = self.mybase.corner4_y

		distanceX = X - tank.x
		distanceY = Y - tank.y
		tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)
		if tot_distance > 5:
			target_angle = math.atan2(distanceY, distanceX)
			relative_angle = self.normalize_angle(target_angle - tank.angle)
			self.bzrc.angvel(tank.index,  0.75 * relative_angle)
			if relative_angle < 0.1:
				self.bzrc.speed(tank.index, tot_distance / 100)
			else:
				self.bzrc.speed(tank.index, 0)
			print "moving"
			return "moving"
		self.bzrc.speed(tank.index, 0)
		print "shooting"
		return "shooting"

	def chooseTarget(self, tank):
#----==== DETERMINE BEST TARGET ====----#
		best_dist = float("inf")
		best_enemy = self.enemies[0]
		for enemy in self.enemies:
			if enemy.status != 'alive':
				continue
			tot_distance = math.sqrt((tank.x - mu[enemy.callsign][0])**2 + (tank.y - mu[enemy.callsign][3])**2)

			if tot_distance < best_dist:
				#print "FOUND BETTER ENEMY"
				best_dist = tot_distance
				best_enemy = enemy

		#if(best_enemy.status != 'alive'):
		#	continue

		return best_dist, best_enemy

	def predict(self, tank, best_dist, best_enemy):
		global dt
		target = numpy.matrix([[0],[0]])

		current_mu = mu[best_enemy.callsign]
		time = best_dist / self.bulletSpeed #Time = time for bullet to reach target (approx)
		time = math.floor(time / dt)
		#print time
		i = 0
		new_mu = mu[best_enemy.callsign]
		while i < time:
			new_mu = F * new_mu
			i += 1
		target = H * new_mu

		return target

	def tick(self, time_diff):
		global F, S, mu, Sx, Sz, H, dt
		print time_diff	
		"""Some time has passed; decide what to do next."""
		mytanks, othertanks, flags, shots = self.bzrc.get_lots_o_stuff()
		self.mytanks = mytanks
		self.othertanks = othertanks
		self.flags = flags
		self.shots = shots
		self.enemies = [tank for tank in othertanks if tank.color !=
						self.constants['team']]

		#---=== UPDATE KALMAN FOR INVADING ENEMY TANKS ===---#	
		self.updateKalman()		

		print "checking tanks" 
		for tank in mytanks:
			if tank.status == 'alive' and tank.index < 4:

				status = self.moveToPost(tank)
				if status == "moving":
					continue

				best_dist, best_enemy = self.chooseTarget(tank)
		
		#----==== CALCULATE FUTURE ENEMY POSITION ====----#
				target = self.predict(tank, best_dist, best_enemy)
				
		#----==== TURN TO FACE PREDICTED TARGET POSITION ====----#
				distanceX = target[0] - tank.x
				distanceY = target[1] - tank.y
				target_angle = math.atan2(distanceY, distanceX)
				relative_angle = self.normalize_angle(target_angle - tank.angle)

		#----==== IF WITHIN A SMALL ANGLE OF TARGET POSITION, FIRE ====----#
				self.bzrc.angvel(tank.index,  0.75 * relative_angle)
				if math.fabs(relative_angle) < 0.05:
					self.bzrc.shoot(tank.index)
					self.bzrc.shoot(tank.index)
					self.bzrc.shoot(tank.index)		

	def normalize_angle(self, angle):
		"""Make any angle be between +/- pi."""
		angle -= 2 * math.pi * int (angle / (2 * math.pi))
		if angle <= -math.pi:
			angle += 2 * math.pi
		elif angle > math.pi:
			angle -= 2 * math.pi
		return angle

def main():
	# Process CLI arguments.
	try:
		execname, host, port = sys.argv
	except ValueError:
		execname = sys.argv[0]
		print >>sys.stderr, '%s: incorrect number of arguments' % execname
		print >>sys.stderr, 'usage: %s hostname port' % sys.argv[0]
		sys.exit(-1)

	# Connect.
	#bzrc = BZRC(host, int(port), debug=True)
	bzrc = BZRC(host, int(port))

	agent = Agent(bzrc)

	prev_time = time.time()

	# Run the agent
	try:
			while (1):
				time_diff = time.time() - prev_time
				if(time_diff < 0.5):
					continue
				prev_time = time.time()
				agent.tick(time_diff)

	except KeyboardInterrupt:
		print "Exiting due to keyboard interrupt."
		bzrc.close()

if __name__ == '__main__':
	main()
