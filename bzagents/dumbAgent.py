#!/usr/bin/python -tt

# An incredibly simple agent.  All we do is find the closest enemy tank, drive
# towards it, and shoot.  Note that if friendly fire is allowed, you will very
# often kill your own tanks with this code.

#################################################################
# NOTE TO STUDENTS
# This is a starting point for you.  You will need to greatly
# modify this code if you want to do anything useful.  But this
# should help you to know how to interact with BZRC in order to
# get the information you need.
#
# After starting the bzrflag server, this is one way to start
# this code:
# python agent0.py [hostname] [port]
#
# Often this translates to something like the following (with the
# port name being printed out by the bzrflag server):
# python agent0.py localhost 49857
#################################################################

import sys
import math
import time
import random

from bzrc import BZRC, Command
from random import randint

class Agent(object):
	"""Class handles all command and control logic for a teams tanks."""

	def __init__(self, bzrc):
		self.bzrc = bzrc
		self.constants = self.bzrc.get_constants()
		self.commands = []
		self.movement_timer = 0
                self.target_turn_time = 0
                self.needs_target_turn_time = True
                self.target_shoot_time = 0
                self.needs_target_shoot_time = True
		self.shoot_timer = 0
		self.has_target_angle = False
		self.target_angle = [0,0,0,0,0,0,0,0,0,0]

	def tick(self, time_diff):
                if self.needs_target_turn_time:
                    self.needs_target_turn_time = False
                    self.target_turn_time = randint(3,8)
                if self.needs_target_shoot_time:
                    self.needs_target_shoot_time = False
                    self.target_shoot_time = random.uniform(1.5,2.5)
		self.movement_timer += time_diff
		self.shoot_timer += time_diff
			
		"""Some time has passed; decide what to do next."""
		mytanks, othertanks, flags, shots = self.bzrc.get_lots_o_stuff()
		self.mytanks = mytanks
		self.othertanks = othertanks
		self.flags = flags
		self.shots = shots
		self.enemies = [tank for tank in othertanks if tank.color !=
						self.constants['team']]
		
		if self.has_target_angle:
			"""Perform rotation"""
			for tank in mytanks:
				if tank.status != 'alive':	  #Tanks that are dead shouldn't keep turning to their last angle
					self.target_angle[tank.index] = tank.angle
				relative_angle = self.normalize_angle(self.target_angle[tank.index] - tank.angle)
				self.bzrc.angvel(tank.index, relative_angle)
				#self.bzrc.angvel(tank.index, 0)
				#print 'TANK %s tar angle: %s' %(tank.index, self.target_angle[tank.index])
				#print 'TANK %s rel angle: %s' %(tank.index, relative_angle)
				print 'Tank %s status:%s' %(tank.index, tank.status)
				if relative_angle < 0.01 and relative_angle > -0.01:					
					#self.target_angle[tank.index] = tank.angle
					#print "Finish Turning"
					self.has_target_angle = False
				else:
					self.has_target_angle = True	#If any tank has not finished turning, wait another tick
			if not self.has_target_angle:
				self.movement_timer = 0				#If all tanks are done turning, set timer back to 0

		else:
			for tank in mytanks:
				self.bzrc.angvel(tank.index, 0.0)
			if self.movement_timer > self.target_turn_time:
                                self.needs_target_turn_time = True				
				"""Set target angles to tank angle + 60 degrees"""
				self.has_target_angle = True
				for tank in mytanks:
					self.target_angle[tank.index] = tank.angle + math.pi / 3
					relative_angle = self.normalize_angle(self.target_angle[tank.index] - tank.angle)
					print 'TANK %s rel angle: %s' %(tank.index, relative_angle)
					
		
		
		for tank in mytanks:
			self.bzrc.speed(tank.index, 1.0)	
			
		if self.shoot_timer > self.target_shoot_time:
                        self.needs_target_shoot_time = True
			self.shoot_timer = 0
			for tank in mytanks:
				self.bzrc.shoot(tank.index)
		self.commands = []

		results = self.bzrc.do_commands(self.commands)

	def attack_enemies(self, tank):
		"""Find the closest enemy and chase it, shooting as you go."""
		best_enemy = None
		best_dist = 2 * float(self.constants['worldsize'])
		for enemy in self.enemies:
			if enemy.status != 'alive':
				continue
			dist = math.sqrt((enemy.x - tank.x)**2 + (enemy.y - tank.y)**2)
			if dist < best_dist:
				best_dist = dist
				best_enemy = enemy
		if best_enemy is None:
			command = Command(tank.index, 0, 0, False)
			self.commands.append(command)
		else:
			self.move_to_position(tank, best_enemy.x, best_enemy.y)

	def move_to_position(self, tank, target_x, target_y):
		"""Set command to move to given coordinates."""
		target_angle = math.atan2(target_y - tank.y,
								  target_x - tank.x)
		relative_angle = self.normalize_angle(target_angle - tank.angle)
		command = Command(tank.index, 1, 2 * relative_angle, True)
		self.commands.append(command)

	def normalize_angle(self, angle):
		"""Make any angle be between +/- pi."""
		angle -= 2 * math.pi * int (angle / (2 * math.pi))
		if angle <= -math.pi:
			angle += 2 * math.pi
		elif angle > math.pi:
			angle -= 2 * math.pi
		return angle


def main():
	# Process CLI arguments.
	try:
		execname, host, port = sys.argv
	except ValueError:
		execname = sys.argv[0]
		print >>sys.stderr, '%s: incorrect number of arguments' % execname
		print >>sys.stderr, 'usage: %s hostname port' % sys.argv[0]
		sys.exit(-1)

	# Connect.
	#bzrc = BZRC(host, int(port), debug=True)
	bzrc = BZRC(host, int(port))

	agent = Agent(bzrc)

	prev_time = time.time()

	# Run the agent
	try:
		while True:
			time_diff = time.time() - prev_time
			prev_time = time.time()
			agent.tick(time_diff)
	except KeyboardInterrupt:
		print "Exiting due to keyboard interrupt."
		bzrc.close()


if __name__ == '__main__':
	main()

# vim: et sw=4 sts=4
