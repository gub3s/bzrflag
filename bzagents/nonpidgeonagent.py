#!/usr/bin/python -tt

# An incredibly simple agent.  All we do is find the closest enemy tank, drive
# towards it, and shoot.  Note that if friendly fire is allowed, you will very
# often kill your own tanks with this code.

#################################################################
# NOTE TO STUDENTS
# This is a starting point for you.  You will need to greatly
# modify this code if you want to do anything useful.  But this
# should help you to know how to interact with BZRC in order to
# get the information you need.
#
# After starting the bzrflag server, this is one way to start
# this code:
# python agent0.py [hostname] [port]
#
# Often this translates to something like the following (with the
# port name being printed out by the bzrflag server):
# python agent0.py localhost 49857
#################################################################

import sys
import math
import time
import random

from bzrc import BZRC, Command
from random import randint

class Agent(object):
	"""Class handles all command and control logic for a teams tanks."""

	#10_DEGREES = 0.1745

	def __init__(self, bzrc):
		self.bzrc = bzrc
		self.constants = self.bzrc.get_constants()
		self.commands = []
		self.movement_timer = 0
		self.target_turn_time = 0
		self.needs_target_turn_time = True
		self.target_shoot_time = 0
		self.needs_target_shoot_time = True
		self.shoot_timer = 0
		self.randomwait = 0
		self.target_flag = ['','','','','','','','','','']
		self.myspeed = 1
		self.myangle = 0
		#self.obstacles = self.bzrc.get_obstacles()
		bases = self.bzrc.get_bases()
		for base in bases:
			if base.color == self.constants['team']:
				self.mybase = base
				break		

	def tick(self, time_diff):
		self.shoot_timer += time_diff
			
		"""Some time has passed; decide what to do next."""
		mytanks, othertanks, flags, shots = self.bzrc.get_lots_o_stuff()
		self.mytanks = mytanks
		self.othertanks = othertanks
		self.flags = flags
		self.shots = shots
		self.enemies = [tank for tank in othertanks if tank.color !=
						self.constants['team']]

		print "checking tanks"
		for tank in mytanks:
			if tank.status != 'alive':
				self.myspeed = random.random()
				continue
			if self.randomwait <= 0:
				self.randomwait = random.randint(2, 8)	   #Every random 4 - 10 ticks, (2 - 5 seconds) change turning and speed
				self.myspeed = random.random() * 0.5 + 0.5
				'''if newspeed < 0.0 and newspeed > -0.99:
					newspeed = -1.0
				elif newspeed > 0.0 and newspeed < 0.99:
					newspeed = 1.0'''
				self.myangle = random.random() * 2.0 - 1.0
			else:
				self.randomwait -= 1

			distanceX = 0 - tank.x
			distanceY = 0 - tank.y
			target_angle = math.atan2(distanceY, distanceX)
			relative_angle = self.normalize_angle(target_angle - tank.angle)
			tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)

			if tot_distance > 200:   #If you are very far from center, get pulled toward it
				Xdir = distanceX / tot_distance
				Ydir = distanceY / tot_distance
				self.myspeed = 0.3 * self.myspeed + 0.7 * math.sqrt(Xdir ** 2 + Ydir ** 2)
				self.myangle *= 0.2 * self.myangle + relative_angle  
			
			self.bzrc.angvel(tank.index, self.myangle)
			self.bzrc.speed(tank.index, self.myspeed)
			print self.myspeed
			'''elif tot_distance > 75.0:   #Otherwise, slow down the closer you get
				Xdir += (distanceX / tot_distance) * (tot_distance / flag_rad)
				Ydir += (distanceY / tot_distance) * (tot_distance / flag_rad)'''

	def normalize_angle(self, angle):
		"""Make any angle be between +/- pi."""
		angle -= 2 * math.pi * int (angle / (2 * math.pi))
		if angle <= -math.pi:
			angle += 2 * math.pi
		elif angle > math.pi:
			angle -= 2 * math.pi
		return angle

def main():
	# Process CLI arguments.
	try:
		execname, host, port = sys.argv
	except ValueError:
		execname = sys.argv[0]
		print >>sys.stderr, '%s: incorrect number of arguments' % execname
		print >>sys.stderr, 'usage: %s hostname port' % sys.argv[0]
		sys.exit(-1)

	# Connect.
	#bzrc = BZRC(host, int(port), debug=True)
	bzrc = BZRC(host, int(port))

	agent = Agent(bzrc)

	prev_time = time.time()

	# Run the agent
	try:
		while True:
			time_diff = time.time() - prev_time
			prev_time = time.time()
			agent.tick(time_diff)
	except KeyboardInterrupt:
		print "Exiting due to keyboard interrupt."
		bzrc.close()


if __name__ == '__main__':
	main()

# vim: et sw=4 sts=4
