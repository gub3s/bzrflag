#!/usr/bin/python -tt

# An incredibly simple agent.  All we do is find the closest enemy tank, drive
# towards it, and shoot.  Note that if friendly fire is allowed, you will very
# often kill your own tanks with this code.

#################################################################
# NOTE TO STUDENTS
# This is a starting point for you.  You will need to greatly
# modify this code if you want to do anything useful.  But this
# should help you to know how to interact with BZRC in order to
# get the information you need.
#
# After starting the bzrflag server, this is one way to start
# this code:
# python agent0.py [hostname] [port]
#
# Often this translates to something like the following (with the
# port name being printed out by the bzrflag server):
# python agent0.py localhost 49857
#################################################################

import sys
import math
import time
import random

import numpy
import OpenGL
OpenGL.ERROR_CHECKING = False
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from numpy import zeros

grid = None
WIDTH = 800
HEIGHT = 800
#visited_grid = None
visited_grid = zeros((WIDTH, HEIGHT))

from bzrc import BZRC, Command
from random import randint

class Agent(object):
	"""Class handles all command and control logic for a teams tanks."""

	#10_DEGREES = 0.1745

	def __init__(self, bzrc):
		self.bzrc = bzrc
		self.constants = self.bzrc.get_constants()
		self.tp = float(self.constants['truepositive'])
		self.tn = float(self.constants['truenegative'])
		self.max_thres = 0.95
		self.min_thres = 0.05
		self.commands = []
		self.sampled_point = (-370,0)
		self.target_flag = ['','','','','','','','','','']
		#self.obstacles = self.bzrc.get_obstacles()
		init_window(800,800)
		
		self.has_target = [True, True, True, True, True, True, True, True, True, True]
		self.target = [(0, 0),(799,799), (0, 799), (799, 0), (123,234), (345,456), (567,678), (765,654), (543,432), (765,234)]

	def tick(self, time_diff):
		#self.shoot_timer += time_diff
			
		"""Some time has passed; decide what to do next."""
		mytanks, othertanks, flags, shots = self.bzrc.get_lots_o_stuff()
		self.mytanks = mytanks
		self.othertanks = othertanks
		self.flags = flags
		self.shots = shots
		self.enemies = [tank for tank in othertanks]
		for mytank in mytanks:
			if mytank.index > 1:
				self.enemies.append(mytank)
	
		maptanks = []
		maptanks.append(mytanks[0])
		maptanks.append(mytanks[1])
		maptanks.append(mytanks[2])
		maptanks.append(mytanks[3])
		"""maptanks.append(mytanks[4])
		maptanks.append(mytanks[5])
		maptanks.append(mytanks[6])
		maptanks.append(mytanks[7])
		maptanks.append(mytanks[8])"""

						
		for tank in maptanks:
			flag_rad = 100
			enemy_rad = 10
			friend_rad = 100
			base_rad = 100
			obstacle_rad = 50
			Xdir = 0
			Ydir = 0
			gridTx = tank.x + 399
			gridTy = tank.y + 399

		#---=== UPDATE GRID ===---#
			pos, new_view = self.bzrc.get_occgrid(tank.index)
			pos = (pos[0] + 399, pos[1] + 399)
			#pos[1] += 400
			new_view = numpy.array(new_view)
			new_grid = grid
			for (x,y), value in numpy.ndenumerate(new_view):
				#print x, y, new_view[x,y]
				observed = new_view[x, y]
				if not new_grid[pos[1] + y, pos[0] + x] == 1 or new_grid[pos[1] + y, pos[0] + x] == 0:
					grid_pos = grid[pos[1] + y, pos[0] + x]
					
					if observed == 0:
						empty_empty = self.tn * (1-grid_pos)
						empty_wall =  (1 - self.tp) * grid_pos
						new_grid[pos[1] + y, pos[0] + x] =(1 - self.tp) * grid_pos / (empty_empty + empty_wall)
					else:
						wall_empty = (1 - self.tn) * (1-grid_pos)
						wall_wall =  self.tp * grid_pos
						new_grid[pos[1] + y, pos[0] + x] = self.tp * grid_pos / (wall_empty + wall_wall)
					if new_grid[pos[1] + y, pos[0] + x] >= self.max_thres:
						if new_grid[pos[1] + y - 1, pos[0] + x] >= self.max_thres and new_grid[pos[1] + y + 1, pos[0] + x] >= self.max_thres and new_grid[pos[1] + y, pos[0] + x - 1] >= self.max_thres and new_grid[pos[1] + y, pos[0] + x + 1] >= self.max_thres:
							new_grid[pos[1] + y, pos[0] + x] = 1
					elif new_grid[pos[1] + y - 1, pos[0] + x] <= self.min_thres and new_grid[pos[1] + y + 1, pos[0] + x] <= self.min_thres and new_grid[pos[1] + y, pos[0] + x - 1] <= self.min_thres and new_grid[pos[1] + y, pos[0] + x + 1] <= self.min_thres:
						new_grid[pos[1] + y, pos[0] + x] = 0
					visited_grid[pos[1] + y, pos[0] + x] = 1
					
					if pos[1] + y == self.sampled_point[1] and pos[0] + x == self.sampled_point[0]:
						print observed
						print new_grid[pos[1] + y, pos[0] + x]
			#new_grid[gridTy, gridTx] = 0.5	  #Draw tank on grid
			#new_grid[gridTy, gridTx] = 2
			#visited_grid[gridTy, gridTx] = 0.5  #Draw tank on visited Grid
					
			update_grid(new_grid)
			#draw_grid(new_grid)

			#draw_grid(new_grid)

		#---=== LOOK FOR UNMAPPED PIXELS ===---#
			#print visited_grid
			middleX = len(new_view) / 2
			middleY = len(new_view[0]) / 2
			#print middleX
			#print middleY
			if visited_grid[self.target[tank.index][1], self.target[tank.index][0]] == 1.0:
				self.has_target[tank.index] = False
			if not self.has_target[tank.index]:
				for x in range(int(gridTx - 5 * middleX), int(gridTx + 5 * middleX)):
					if x < 5 or x > WIDTH - 6 or (x < gridTx + 2 * middleX and x > gridTx - 2 * middleX):
						continue
					for y in range(int(gridTy - 5 * middleY), int(gridTy + 5 * middleY)):
						if y < 5 or y > WIDTH - 6 or (y < gridTy + 2 * middleY and y > gridTy - 2 * middleY):
							continue
						if visited_grid[y, x] < 1:
							#print "setting new target"
							self.target[tank.index] = (x, y)
							self.has_target[tank.index] = True
							break
					if self.has_target[tank.index] == True:
						break
			if not self.has_target[tank.index]:
				for x in range(5, 794):
					for y in range(5, 794):
						if visited_grid[y, x] < 1:
							#print "setting new target"
							self.target[tank.index] = (x, y)
							self.has_target[tank.index] = True
							break
					if self.has_target[tank.index] == True:
						break					
		
			#print "Target: %s %s" % self.target
			#print "Target Value: %s" % visited_grid[self.target[tank.index][1], self.target[tank.index][0]]
			tX = self.target[tank.index][0] - WIDTH/2
			tY = self.target[tank.index][1] - HEIGHT/2
			#print "Tank: %s" % tank.x, tank.y
			#print "Target: %s" % tX, tY
			distanceX = tX - tank.x
			distanceY = tY - tank.y
			tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)
			target_angle = math.atan2(distanceX, distanceY)
			relative_angle = self.normalize_angle(target_angle - tank.angle)

			if tot_distance > flag_rad: #If you are very far from flag, go full speed
				Xdir += distanceX / tot_distance
				Ydir += distanceY / tot_distance

			elif tot_distance > 75.0:   #Otherwise, slow down the closer you get
				Xdir += (distanceX / tot_distance) * (tot_distance / flag_rad)
				Ydir += (distanceY / tot_distance) * (tot_distance / flag_rad)

			else:   #If you are very very close, this is the minimum speed
				Xdir += (distanceX / (tot_distance + 0.1)) * (75.0 / flag_rad)
				Ydir += (distanceY / (tot_distance + 0.1)) * (75.0 / flag_rad)

		#---=== DO MOVEMENT ===---# 
			#for y in range(middleY):
			#	for x in range(middleX):
			#		if x == 0 and y == 0:
			minimum = min(middleY, middleX)
			distanceX = 0
			distanceY = 0
			numwalls = 0
			pushX = 0
			pushY = 0
			#print "MINIMUM %s" % minimum
			for s in range(minimum):
				#print s
				if s == 0:	
					continue
				if gridTx + s > WIDTH - 1 or gridTy + s > WIDTH - 1:
					break
				if grid[gridTy - s][gridTx - s] == 1:
					distanceX = -s
					distanceY = -s
					#print "push up right"
				elif grid[gridTy - s][gridTx + s] == 1:
					distanceX = s
					distanceY = -s
					#print "push up left"
				elif grid[gridTy + s][gridTx - s] == 1:
					distanceX = -s
					distanceY = s
					#print "push down right"
				elif grid[gridTy + s][gridTx + s] == 1:
					distanceX = s
					distanceY = s   
					#print "push down left"

				tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)
				if tot_distance == 0:
					continue

				if tot_distance > enemy_rad:	#If you are too far away, don't worry
					continue	
				target_angle = math.atan2(distanceY, distanceX)
				pushX += math.cos(target_angle + math.pi / 2)
				pushY += math.sin(target_angle + math.pi / 2)
				pushX -= 2 * (distanceX / tot_distance )* (obstacle_rad - tot_distance) / obstacle_rad
				pushY -= 2 * (distanceY / tot_distance) * (obstacle_rad - tot_distance) / obstacle_rad
				numwalls += 1

			if numwalls != 0:
				Xdir /= 10
				Ydir /= 10	
				Xdir += 10 * pushX / numwalls
				Ydir += 10 * pushY / numwalls
				#self.has_target[tank.index] = False
			#print "Xdir %s" % Xdir
			#print "Ydir %s" % Ydir

			target_angle = math.atan2(Ydir, Xdir)
			relative_angle = self.normalize_angle(target_angle - tank.angle)
			self.bzrc.angvel(tank.index, relative_angle)
			target_velocity = math.sqrt(Xdir ** 2 + Ydir ** 2)
			if target_velocity > 1:
				target_velocity = 1
			target_velocity *= (math.pi - 1 * math.fabs(relative_angle)) / math.pi #if you still need to turn, slow down a bit
			if target_velocity < 0.6:
				target_velocity = 0.6
			self.bzrc.speed(tank.index, target_velocity)
			#self.bzrc.shoot(tank.index)

	def normalize_angle(self, angle):
		"""Make any angle be between +/- pi."""
		angle -= 2 * math.pi * int (angle / (2 * math.pi))
		if angle <= -math.pi:
			angle += 2 * math.pi
		elif angle > math.pi:
			angle -= 2 * math.pi
		return angle

def draw_grid(the_grid):
	# This assumes you are using a numpy array for your grid
	width, height = the_grid.shape
	glRasterPos2f(-1, -1)
	glDrawPixels(width, height, GL_LUMINANCE, GL_FLOAT, the_grid)
	glFlush()
	glutSwapBuffers()

def update_grid(new_grid):
	global grid
	grid = new_grid

def init_window(width, height):
	global window
	global grid
	grid = zeros((width, height))
	for (x,y), value in numpy.ndenumerate(grid):
		grid[x][y] = 0.15
	#glutInit(())
	#glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)
	#glutInitWindowSize(width, height)
	#glutInitWindowPosition(0, 0)
	#window = glutCreateWindow("Grid filter")
	#glutDisplayFunc(draw_grid)
	#glMatrixMode(GL_PROJECTION)
	#glLoadIdentity()
	#glMatrixMode(GL_MODELVIEW)
	#glLoadIdentity()
	#glutMainLoop()

def main():
	# Process CLI arguments.
	try:
		execname, host, port = sys.argv
	except ValueError:
		execname = sys.argv[0]
		print >>sys.stderr, '%s: incorrect number of arguments' % execname
		print >>sys.stderr, 'usage: %s hostname port' % sys.argv[0]
		sys.exit(-1)

	# Connect.
	#bzrc = BZRC(host, int(port), debug=True)
	bzrc = BZRC(host, int(port))

	agent = Agent(bzrc)

	prev_time = time.time()

	# Run the agent
	try:
		while True:
			time_diff = time.time() - prev_time
			prev_time = time.time()
			agent.tick(time_diff)
	except KeyboardInterrupt:
		print "Exiting due to keyboard interrupt."
		bzrc.close()


if __name__ == '__main__':
	main()

# vim: et sw=4 sts=4
