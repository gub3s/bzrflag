#!/usr/bin/python -tt

# An incredibly simple agent.  All we do is find the closest enemy tank, drive
# towards it, and shoot.  Note that if friendly fire is allowed, you will very
# often kill your own tanks with this code.

#################################################################
# NOTE TO STUDENTS
# This is a starting point for you.  You will need to greatly
# modify this code if you want to do anything useful.  But this
# should help you to know how to interact with BZRC in order to
# get the information you need.
#
# After starting the bzrflag server, this is one way to start
# this code:
# python agent0.py [hostname] [port]
#
# Often this translates to something like the following (with the
# port name being printed out by the bzrflag server):
# python agent0.py localhost 49857
#################################################################

import sys
import math
import time
import random

import numpy
numpy.set_printoptions(threshold='nan')
#import OpenGL
#OpenGL.ERROR_CHECKING = False
#from OpenGL.GL import *
#from OpenGL.GLUT import *
#from OpenGL.GLU import *
from numpy import zeros

grid = None
WIDTH = 800
HEIGHT = 800

#Radius at which potential field logic occurs
flag_rad = 100
enemy_rad = 40
friend_rad = 100
base_rad = 100
obstacle_rad = 20

#visited_grid = None
visited_grid = zeros((WIDTH, HEIGHT))

from bzrc import BZRC, Command
from random import randint

class Agent(object):
	"""Class handles all command and control logic for a teams tanks."""

	#10_DEGREES = 0.1745

	def __init__(self, bzrc):
		self.bzrc = bzrc
		self.constants = self.bzrc.get_constants()
		self.tp = float(self.constants['truepositive'])
		self.tn = float(self.constants['truenegative'])
		self.max_thres = 0.95
		self.min_thres = 0.05
		self.commands = []
		self.scanTimer = 2
		self.bulletSpeed = float(self.constants['shotspeed'])
		self.alive = 100

		init_window(800,800)
		
		bases = self.bzrc.get_bases()
		for base in bases:
			if base.color == self.constants['team']:
				self.mybase = base
				break

		self.myBaseX = 0.5 * (self.mybase.corner1_x + self.mybase.corner3_x)
		self.myBaseY = 0.5 * (self.mybase.corner1_y + self.mybase.corner3_y)

		mytanks, othertanks, flags, shots = self.bzrc.get_lots_o_stuff()
		self.enemies = [tank for tank in othertanks if tank.color !=
				self.constants['team']]

		global dt, F, Sx, H, Sz, mu, S
		dt = 0.5
		mu = {}
		S = {}
		
		for enemy in self.enemies:
			#print enemy.callsign
			#---=== Constant Matrices; Calculate once ===---#

			F = numpy.matrix([[1, dt, dt*dt/2, 0, 0, 0],	#Physics equations
							  [0, 1, dt, 0, 0, 0],
							  [0, 0, 1, 0, 0, 0],
							  [0, 0, 0, 1, dt, dt*dt/2],
							  [0, 0, 0, 0, 1, dt],
							  [0, 0, 0, 0, 0, 1]])

			Sx = numpy.matrix([[0.1, 0, 0, 0, 0, 0],  #Uncertainty in physics
							   [0, 0.1, 0, 0, 0, 0],
							   [0, 0, 0.5, 0, 0, 0],
							   [0, 0, 0, 0.1, 0, 0],
							   [0, 0, 0, 0, 0.1, 0],
							   [0, 0, 0, 0, 0, 0.5]])				

			H = numpy.matrix([[1, 0, 0, 0, 0, 0],	#Pick out X and Y
							  [0, 0, 0, 1, 0, 0]])

			Sz = numpy.matrix([[9, 0],	 #Uncertainty in X, Y sensor
							   [0, 9]])
				#---=== =============================== === ---#

				#---=== To be updated each tick; But initialize some guesses ===---#
			mu[enemy.callsign] = numpy.matrix([[0], #x	  #Estimate of tank state
								[0], #x'
								[0], #x''
								[0], #y
								[0], #y'
								[0]])#y''

			S[enemy.callsign] = numpy.matrix([[100, 0, 0, 0, 0, 0], #Uncertainty in estimate
							  [0, 0.1, 0, 0, 0, 0],
							  [0, 0, 0.1, 0, 0, 0],
							  [0, 0, 0, 100, 0, 0],
							  [0, 0, 0, 0, 0.1, 0],
							  [0, 0, 0, 0, 0, 0.1]])
				#---=== =============================== === ---#

	def avoidWalls(self, tank, Xdir, Ydir):
	#---=== AVOID OBSTACLES ===---#
		distanceX = 0
		distanceY = 0
		numwalls = 0
		pushX = 0
		pushY = 0
		gridTx = tank.x + 399
		gridTy = tank.y + 399
		#print grid
		#print "MINIMUM %s" % minimum
		for s in range(obstacle_rad):
			#print s
			if s == 0:	
				continue
			if gridTx + s > WIDTH - 1 or gridTy + s > WIDTH - 1:
				break
			if grid[gridTy - s][gridTx - s] == 1:
				distanceX = -s
				distanceY = -s
				#print "push up right"
			if grid[gridTy - s][gridTx + s] == 1:
				distanceX = s
				distanceY = -s
				#print "push up left"
			if grid[gridTy + s][gridTx - s] == 1:
				distanceX = -s
				distanceY = s
				#print "push down right"
			if grid[gridTy + s][gridTx + s] == 1:
				distanceX = s
				distanceY = s   
				#print "push down left"

			tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)
			if tot_distance == 0:
				continue

			#if tot_distance > obstacle_rad:	#If you are too far away, don't worry
			#	continue

			target_angle = math.atan2(distanceY, distanceX)
			pushX += math.cos(target_angle + math.pi / 2)
			pushY += math.sin(target_angle + math.pi / 2)
			pushX -= 2 * (distanceX / tot_distance )* (obstacle_rad - tot_distance) / obstacle_rad
			pushY -= 2 * (distanceY / tot_distance) * (obstacle_rad - tot_distance) / obstacle_rad
			numwalls += 1
			break

		if numwalls != 0:			#If at least one wall was found, slow down by 1/10 and get pushed
			#print "get pushed"
			Xdir /= 5
			Ydir /= 5	
			Xdir += 1 * pushX / numwalls
			Ydir += 1 * pushY / numwalls

		return (Xdir, Ydir)

	def goToFlag(self, tank, flags, Xdir, Ydir):
	#---=== FIND THE CLOSEST ENEMY FLAG, OR YOUR OWN FLAG ===---#
		closest_flag_dist = 12000
		target_flag = 'none'
		if tank.flag == '-':
			'''Get Flag potentials'''
			for flag in flags:
				if flag.color == self.constants['team'] and flag.poss_color == 'none':	#don't go to your own flag unless the enemy has it.
					continue
				if flag.poss_color == self.constants['team']:   #if your team has this flag find another one
					continue
				distanceX = flag.x - tank.x
				distanceY = flag.y - tank.y
				tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)
				if tot_distance < closest_flag_dist:
					closest_flag_dist = tot_distance
					target_flag = flag.color

			if target_flag == 'none':					#If you can't target any flag, just guard your own flag
				target_flag = self.constants['team']

			for flag in flags:
				if flag.color != target_flag:
					continue
				distanceX = flag.x - tank.x
				distanceY = flag.y - tank.y
				#target_angle = math.atan2(distanceY, distanceX)
				#relative_angle = self.normalize_angle(target_angle - tank.angle)
				tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)

			if tot_distance > flag_rad: #If you are very far from target, go full speed
				Xdir += (0.9 + 0.2 * random.random()) * distanceX / tot_distance
				Ydir += (0.9 + 0.2 * random.random()) * distanceY / tot_distance

			elif tot_distance > 75.0:   #Otherwise, slow down the closer you get
				Xdir += (distanceX / tot_distance) * (tot_distance / flag_rad)
				Ydir += (distanceY / tot_distance) * (tot_distance / flag_rad)

			else:   #If you are very very close, this is the minimum speed
				Xdir += (distanceX / (tot_distance + 0.1)) * (75.0 / flag_rad)
				Ydir += (distanceY / (tot_distance + 0.1)) * (75.0 / flag_rad)
	
	#---=== IF YOU GOT A FLAG, RUN BACK TO BASE ===---#
		else:   
			target_flag = ''
			'''Get Base Potential'''
			distanceX = 0.5 * (self.mybase.corner1_x + self.mybase.corner3_x) - tank.x
			distanceY = 0.5 * (self.mybase.corner1_y + self.mybase.corner3_y) - tank.y
			tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)
			
			if tot_distance > base_rad: #If you are very far from flag, go full speed
				Xdir += (0.9 + 0.2 * random.random()) * distanceX / tot_distance
				Ydir += (0.9 + 0.2 * random.random()) * distanceY / tot_distance

			elif tot_distance > 100.0:   #Otherwise, slow down the closer you get
				Xdir += (distanceX / tot_distance) * (tot_distance / base_rad)
				Ydir += (distanceY / tot_distance) * (tot_distance / base_rad)

			else:   #If you are very very close, this is the minimum speed
				#print "DISTANCE < 15: %s" % tot_distance
				Xdir += (distanceX / tot_distance) * (100.0 / base_rad)
				Ydir += (distanceY / tot_distance) * (100.0 / base_rad)

		return (Xdir, Ydir)

	def avoidEnemies(self, tank, Xdir, Ydir):
		for enemy in self.enemies:				
			distanceX = enemy.x - tank.x
			distanceY = enemy.y - tank.y
			target_angle = math.atan2(distanceY, distanceX)
			relative_angle = self.normalize_angle(target_angle - tank.angle)
			tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)

			if tot_distance > enemy_rad:	#If you are too far away, don't worry
				continue
			if enemy.flag != '-':   #If the enemy has the flag you are trying to get, don't run away
				continue
			#if tank.flag != '-':			#If you have the flag just run
			#	continue

			Xdir -= (distanceX / tot_distance) * (enemy_rad - tot_distance) / enemy_rad
			Ydir -= (distanceY / tot_distance) * (enemy_rad - tot_distance) / enemy_rad

		return (Xdir, Ydir)

	def doScan(self, tank):
		#global grid, visited_grid
		#print grid		
		Xdir = 0
		Ydir = 0
		gridTx = tank.x + 399
		gridTy = tank.y + 399

	#---=== IF THIS SPOT IS ALREADY SCANNED, DON'T BOTHER ===---#
		#print grid[gridTx, gridTy]
		#print grid
		if grid[gridTx, gridTy] == 0 or grid[gridTx, gridTy] == 1:
			print "ALREADY SCANNED"
			#print grid
			return
		#print "NOT SCANNED"

	#---=== UPDATE GRID ===---#
		pos, new_view = self.bzrc.get_occgrid(tank.index)
		pos = (pos[0] + 399, pos[1] + 399)
		#pos[1] += 400
		new_view = numpy.array(new_view)
		new_grid = grid
		for (x,y), value in numpy.ndenumerate(new_view):
			#print x, y, new_view[x,y]
			observed = new_view[x, y]
			if not (new_grid[pos[1] + y, pos[0] + x] == 1 or new_grid[pos[1] + y, pos[0] + x] == 0):
				grid_pos = grid[pos[1] + y, pos[0] + x]
				#print "GRID POS: %f" % grid_pos
				if observed == 0:
					empty_empty = self.tn * (1-grid_pos)
					empty_wall =  (1 - self.tp) * grid_pos
					new_grid[pos[1] + y, pos[0] + x] =(1 - self.tp) * grid_pos / (empty_empty + empty_wall)
				else:
					wall_empty = (1 - self.tn) * (1-grid_pos)
					wall_wall =  self.tp * grid_pos
					new_grid[pos[1] + y, pos[0] + x] = self.tp * grid_pos / (wall_empty + wall_wall)

				if new_grid[pos[1] + y, pos[0] + x] >= self.max_thres:
					if new_grid[pos[1] + y - 1, pos[0] + x] >= self.max_thres and new_grid[pos[1] + y + 1, pos[0] + x] >= self.max_thres and new_grid[pos[1] + y, pos[0] + x - 1] >= self.max_thres and new_grid[pos[1] + y, pos[0] + x + 1] >= self.max_thres:
						new_grid[pos[1] + y, pos[0] + x] = 1
				elif new_grid[pos[1] + y - 1, pos[0] + x] <= self.min_thres and new_grid[pos[1] + y + 1, pos[0] + x] <= self.min_thres and new_grid[pos[1] + y, pos[0] + x - 1] <= self.min_thres and new_grid[pos[1] + y, pos[0] + x + 1] <= self.min_thres:
					new_grid[pos[1] + y, pos[0] + x] = 0
				visited_grid[pos[1] + y, pos[0] + x] = 1
				
		update_grid(new_grid)

	def updateKalman(self):
		global F, S, mu, Sx, Sz, H, dt
		for enemy in self.enemies:
			if(enemy.status != 'alive'):  #If enemy is dead, skip calculations
				continue

			distanceX = self.myBaseX - mu[enemy.callsign][0]
			distanceY = self.myBaseY - mu[enemy.callsign][3]
			tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)
			if tot_distance > 1.5 * float(self.constants['shotrange']):		#If enemy out of range, reset kalman
				mu[enemy.callsign] = numpy.matrix([[0], #x	 
								   [0], #x'
								   [0], #x''
								   [0], #y
								   [0], #y'
								   [0]])#y''

				S[enemy.callsign] = numpy.matrix([[100, 0, 0, 0, 0, 0], #Uncertainty in estimate
								  [0, 0.1, 0, 0, 0, 0],
								  [0, 0, 0.1, 0, 0, 0],
								  [0, 0, 0, 100, 0, 0],
								  [0, 0, 0, 0, 0.1, 0],
								  [0, 0, 0, 0, 0, 0.1]])
				continue

			#Observation of enemy state
			Z = numpy.matrix([[enemy.x], #x	  
							[enemy.y]]) #y			
		
		# ---=== UPDATE MATRICES ===--- #						
			FSFTSX = F * S[enemy.callsign] * F.T + Sx
			K = (FSFTSX)*(H.T)*(H*(FSFTSX)*H.T + Sz).I		
			mu[enemy.callsign] = F*mu[enemy.callsign] + K * (Z - H*F*mu[enemy.callsign])
			S[enemy.callsign] = (numpy.identity(6) - K*H)*(FSFTSX)

	def moveToPost(self, tank):
		#Get distance to post
		#print "DOING MOVE TO POST"


		if tank.index == 0:
			X = self.mybase.corner1_x
			Y = self.mybase.corner1_y
		elif tank.index == 1:
			X = self.mybase.corner2_x
			Y = self.mybase.corner2_y
		elif tank.index == 2:
			X = self.mybase.corner3_x
			Y = self.mybase.corner3_y
		elif tank.index == 3:
			X = self.mybase.corner4_x
			Y = self.mybase.corner4_y

		if X < -200:  #Shift a bit depending which side of the map you are so you aren't right on the corners.
			X += 20
		elif X < 0:
			X -= 60
		elif X > 200:
			X -= 20
		elif X > 0:
			X += 60

		if Y < -200:
			Y += 20
		elif Y < 0:
			Y -= 60
		elif Y > 200:
			Y -= 20
		elif Y > 0:
			Y += 60

		distanceX = X - tank.x
		distanceY = Y - tank.y
		tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)
		if tot_distance > 10:
			Xdir = (distanceX / 50)
			Ydir = (distanceY / 50)

		#---=== Get repulsed from every friendly tank ===---#
			for friend in self.mytanks:
				if tank.index != friend.index:				
					distanceX = friend.x - tank.x
					distanceY = friend.y - tank.y
					tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)

					if tot_distance > 10:	#If you are too far away, don't worry
						continue

					Xdir -= (distanceX / tot_distance) * (10 - tot_distance) / 2
					Ydir -= (distanceY / tot_distance) * (10 - tot_distance) / 2

			target_angle = math.atan2(Ydir, Xdir)
			relative_angle = self.normalize_angle(target_angle - tank.angle)
			tot_distance = math.sqrt(Xdir ** 2 + Ydir ** 2)
			self.bzrc.angvel(tank.index,  0.75 * relative_angle)
			if relative_angle < 0.1:
				self.bzrc.speed(tank.index, tot_distance)
			else:
				self.bzrc.speed(tank.index, 0)
			#print "moving"
			return "moving"
		self.bzrc.speed(tank.index, 0)
		#print "shooting"
		return "shooting"

	def chooseTarget(self, tank):
#----==== DETERMINE BEST TARGET ====----#
		best_dist = float("inf")
		best_enemy = self.enemies[0]
		for enemy in self.enemies:
			if enemy.status != 'alive':
				continue
			tot_distance = math.sqrt((tank.x - mu[enemy.callsign][0])**2 + (tank.y - mu[enemy.callsign][3])**2)

			if tot_distance < best_dist:
				#print "FOUND BETTER ENEMY"
				best_dist = tot_distance
				best_enemy = enemy

		#if(best_enemy.status != 'alive'):
		#	continue

		return best_dist, best_enemy

	def predict(self, tank, best_dist, best_enemy):
		global dt
		target = numpy.matrix([[0],[0]])

		current_mu = mu[best_enemy.callsign]
		time = best_dist / self.bulletSpeed #Time = time for bullet to reach target (approx)
		time = math.ceil(time / dt)
		#print time
		i = 0
		new_mu = mu[best_enemy.callsign]
		while i < time:
			new_mu = F * new_mu
			i += 1
		target = H * new_mu

		return target

	def tick(self, time_diff):
		print time_diff

		self.scanTimer += 1
			
		"""Some time has passed; decide what to do next."""
		mytanks, othertanks, flags, shots = self.bzrc.get_lots_o_stuff()
		self.mytanks = mytanks
		self.othertanks = othertanks
		self.flags = flags
		self.shots = shots
		self.enemies = [tank for tank in othertanks]
 
		turrettanks = []		#Guard four corners of base and shoot
		scantanks = []		  #Rush the enemy, scanning obstacles as you go

		for tank in mytanks:
			if tank.index < 4:
				turrettanks.append(tank)
			else:
				scantanks.append(tank)

		for tank in scantanks:
			if tank.status == 'alive':
				self.alive += 1

		if self.alive < 4:			  #If there are less than 4 scanners, send out some turrets
			for tank in turrettanks:
				if tank.status == 'alive':
					turrettanks.remove(tank)
					scantanks.append(tank)
					self.alive += 1
					if self.alive >= 4:
						break

		#print self.alive
		self.alive = 0
			

	#--== SCANNER TANKS ==--#				
		for tank in scantanks:
			if tank.status != 'alive':
				continue
		#---=== Initialize Variables ===---#
			Xdir = 0
			Ydir = 0

		#---=== DECIDE ON A TARGET ===---#
			if self.scanTimer >= 2:	  #Scan only every 2 ticks
				self.doScan(tank)
				self.scanTimer = 0
			(Xdir, Ydir) = self.goToFlag(tank, flags, Xdir, Ydir)

		#---=== DO MOVEMENT ===---# 
			(Xdir, Ydir) = self.avoidWalls(tank, Xdir, Ydir)
			(Xdir, Ydir) = self.avoidEnemies(tank, Xdir, Ydir)
		
			target_angle = math.atan2(Ydir, Xdir)
			relative_angle = self.normalize_angle(target_angle - tank.angle)
			self.bzrc.angvel(tank.index, relative_angle)
			target_velocity = math.sqrt(Xdir ** 2 + Ydir ** 2)
			if target_velocity > 1:
				target_velocity = 1
			target_velocity *= (math.pi - 0.5 * math.fabs(relative_angle)) / math.pi #if you still need to turn, slow down a bit
			#print "velocity %s" % target_velocity
			self.bzrc.speed(tank.index, target_velocity)

			self.bzrc.shoot(tank.index)

			if math.fabs(tank.vx) < 0.1 and math.fabs(tank.vy) < 0.1:		   #or if you are stuck
				self.bzrc.shoot(tank.index)
				self.bzrc.angvel(tank.index, random.uniform(0,1))	   

	#--== TURRET TANKS ==--#
		#---=== UPDATE KALMAN FOR INVADING ENEMY TANKS ===---#	
		self.updateKalman()		

		#print "checking tanks" 
		for tank in turrettanks:
			if tank.status != 'alive':
				continue

			status = self.moveToPost(tank)
			if status == "moving":
				continue

			best_dist, best_enemy = self.chooseTarget(tank)

		#----==== CALCULATE FUTURE ENEMY POSITION ====----#
			target = self.predict(tank, best_dist, best_enemy)
		
		#----==== TURN TO FACE PREDICTED TARGET POSITION ====----#
			distanceX = target[0] - tank.x
			distanceY = target[1] - tank.y
			target_angle = math.atan2(distanceY, distanceX)
			relative_angle = self.normalize_angle(target_angle - tank.angle)

		#----==== IF WITHIN A SMALL ANGLE OF TARGET POSITION, FIRE ====----#
			self.bzrc.angvel(tank.index,  0.75 * relative_angle)
			if math.fabs(relative_angle) < 0.05:
				self.bzrc.shoot(tank.index)

	def normalize_angle(self, angle):
		"""Make any angle be between +/- pi."""
		angle -= 2 * math.pi * int (angle / (2 * math.pi))
		if angle <= -math.pi:
			angle += 2 * math.pi
		elif angle > math.pi:
			angle -= 2 * math.pi
		return angle

def draw_grid(the_grid):
	# This assumes you are using a numpy array for your grid
	width, height = the_grid.shape
	glRasterPos2f(-1, -1)
	glDrawPixels(width, height, GL_LUMINANCE, GL_FLOAT, the_grid)
	glFlush()
	glutSwapBuffers()

def update_grid(new_grid):
	#print "UPDATE GRID"
	#global grid
	grid = new_grid

def init_window(width, height):
	#global window
	global grid
	grid = zeros((width, height))
	for (x,y), value in numpy.ndenumerate(grid):
		grid[x][y] = 0.15

def main():
	# Process CLI arguments.
	try:
		execname, host, port = sys.argv
	except ValueError:
		execname = sys.argv[0]
		print >>sys.stderr, '%s: incorrect number of arguments' % execname
		print >>sys.stderr, 'usage: %s hostname port' % sys.argv[0]
		sys.exit(-1)

	# Connect.
	#bzrc = BZRC(host, int(port), debug=True)
	bzrc = BZRC(host, int(port))

	agent = Agent(bzrc)

	prev_time = time.time()
	ticknum = 0
	# Run the agent
	try:
		while True:
			time_diff = time.time() - prev_time
			if(time_diff < 0.5):
				continue
			prev_time = time.time()
			ticknum += 1
			print ticknum
			agent.tick(time_diff)
	except KeyboardInterrupt:
		print "Exiting due to keyboard interrupt."
		bzrc.close()


if __name__ == '__main__':
	main()

# vim: et sw=4 sts=4
