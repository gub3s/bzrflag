#!/usr/bin/python -tt

# An incredibly simple agent.  All we do is find the closest enemy tank, drive
# towards it, and shoot.  Note that if friendly fire is allowed, you will very
# often kill your own tanks with this code.

#################################################################
# NOTE TO STUDENTS
# This is a starting point for you.  You will need to greatly
# modify this code if you want to do anything useful.  But this
# should help you to know how to interact with BZRC in order to
# get the information you need.
#
# After starting the bzrflag server, this is one way to start
# this code:
# python agent0.py [hostname] [port]
#
# Often this translates to something like the following (with the
# port name being printed out by the bzrflag server):
# python agent0.py localhost 49857
#################################################################

from __future__ import division
import sys
import math
import time
import random
import numpy


from bzrc import BZRC, Command
from random import randint
from itertools import cycle

try:
	from numpy import linspace

except ImportError:
	# This is stolen from numpy.  If numpy is installed, you don't
	# need this:
	def linspace(start, stop, num=50, endpoint=True, retstep=False):
		"""Return evenly spaced numbers.

		Return num evenly spaced samples from start to stop.  If
		endpoint is True, the last sample is stop. If retstep is
		True then return the step value used.
		"""
		num = int(num)
		if num <= 0:
			return []
		if endpoint:
			if num == 1:
				return [float(start)]
			step = (stop-start)/float((num-1))
			y = [x * step + start for x in xrange(0, num - 1)]
			y.append(stop)
		else:
			step = (stop-start)/float(num)
			y = [x * step + start for x in xrange(0, num)]
		if retstep:
			return y, step
		else:
			return y


########################################################################
# Constants

# Output file:
FILENAME = 'fields.gpi'
# Size of the world (one of the "constants" in bzflag):
WORLDSIZE = 800
FIELDSIZE = 200
# How many samples to take along each dimension:
SAMPLES = 50
# Change spacing by changing the relative length of the vectors.  It looks
# like scaling by 0.75 is pretty good, but this is adjustable:
VEC_LEN = 0.75 * WORLDSIZE / SAMPLES
# Animation parameters:
ANIMATION_MIN = 0
ANIMATION_MAX = 500
ANIMATION_FRAMES = 50


########################################################################
# Field and Obstacle Definitions

def generate_field_function(scale):
	def function(x, y):
		'''User-defined field function.'''
		sqnorm = (x**2 + y**2)
		if sqnorm == 0.0:
			return 0, 0
		else:
			return x*scale/sqnorm, y*scale/sqnorm
	return function

########################################################################
# Helper Functions

def gpi_point(x, y, vec_x, vec_y):
	'''Create the centered gpi data point (4-tuple) for a position and
	vector.  The vectors are expected to be less than 1 in magnitude,
	and larger values will be scaled down.'''
	r = (vec_x ** 2 + vec_y ** 2) ** 0.5
	if r > 1:
		vec_x /= r
		vec_y /= r
	return (x + vec_x * VEC_LEN / 2, y + vec_y * VEC_LEN / 2,
			-vec_x * VEC_LEN, -vec_y * VEC_LEN)

def gnuplot_header(minimum, maximum):
    '''Return a string that has all of the gnuplot sets and unsets.'''
    s = ''
    s += 'set xrange [%s: %s]\n' % (minimum, maximum)
    s += 'set yrange [%s: %s]\n' % (minimum, maximum)
    s += 'set pm3d\n'
    s += 'set view map\n'
    # The key is just clutter.  Get rid of it:
    s += 'unset key\n'
    # Make sure the figure is square since the world is square:
    s += 'set size square\n'
    # Add a pretty title (optional):
    #s += "set title 'Potential Fields'\n"
    return s

def draw_line(p1, p2):
    '''Return a string to tell Gnuplot to draw a line from point p1 to
    point p2 in the form of a set command.'''
    x1, y1 = p1
    x2, y2 = p2
    return 'set arrow from %s, %s to %s, %s nohead front lt 3\n' % (x1, y1, x2, y2)

def plot_stuffs(function):
    global mu, points
    s = "set palette model RGB functions 1-gray, 1-gray, 1-gray\n"
    s += "set isosamples 100\n\n"
    #print mu
    s += "mu_x = %f\n" % mu[0]
    s += "mu_y = %f\n" % mu[3]
    s += "sigma_x = 5\n"
    s += "sigma_y = 5\n"
    s += "rho = 0.0\n"
    s += "splot 1.0/(2.0 * pi * sigma_x * sigma_y * sqrt(1 - rho**2) ) \
        * exp(-1.0/2.0 * ((x - mu_x)**2 / sigma_x**2 + (y - mu_y)**2 / sigma_y**2 \
        - 2.0*rho*(x - mu_x)*(y - mu_y)/(sigma_x*sigma_y) ) ) with pm3d"
    for point in points:
        s += ", \"< echo %f %f 0\" w p lt 1 pt 1" % (point[0], point[3])
    s += "\n"

    
    return s


########################################################################

class Agent(object):
	"""Class handles all command and control logic for a teams tanks."""

	#10_DEGREES = 0.1745

	def __init__(self, bzrc):
		self.bzrc = bzrc
		self.constants = self.bzrc.get_constants()
		self.commands = []
		self.movement_timer = 0
		self.target_turn_time = 0
		self.needs_target_turn_time = True
		self.target_shoot_time = 0
		self.needs_target_shoot_time = True
		self.shoot_timer = 0
                self.bulletSpeed = int(self.constants['shotspeed'])
                self.target = numpy.matrix([[0],[0]])
	

	def tick(self, time_diff):
                global F, S, mu, Sx, Sz, H, points
		self.shoot_timer += time_diff
		print time_diff	
		"""Some time has passed; decide what to do next."""
		mytanks, othertanks, flags, shots = self.bzrc.get_lots_o_stuff()
		self.mytanks = mytanks
		self.othertanks = othertanks
		self.flags = flags
		self.shots = shots
		self.enemies = [tank for tank in othertanks if tank.color !=
						self.constants['team']]
		
		print "checking tanks" 
		for tank in mytanks:
			if tank.status == 'alive':
				points = []
                                if(self.enemies[0].status != 'alive'):
                                        points = []
                                        #Estimate of tank state
                                        mu = numpy.matrix([[0], #x	 
					   [0], #x'
					   [0], #x''
					   [0], #y
					   [0], #y'
					   [0]])#y''

                                        S = numpy.matrix([[100, 0, 0, 0, 0, 0], #Uncertainty in estimate
					  [0, 0.1, 0, 0, 0, 0],
					  [0, 0, 0.1, 0, 0, 0],
					  [0, 0, 0, 100, 0, 0],
					  [0, 0, 0, 0, 0.1, 0],
					  [0, 0, 0, 0, 0, 0.1]])

                                        self.target = numpy.matrix([[0],[0]])
                                        continue
				enemy = self.enemies[0]
                                #Observation of enemy state
				Z = numpy.matrix([[enemy.x], #x	  
					  [enemy.y]]) #y			
                        # ---=== UPDATE MATRICES ===--- #
                                
				FSFTSX = F * S * F.T + Sx
				K = (FSFTSX)*(H.T)*(H*(FSFTSX)*H.T + Sz).I		
				mu = F*mu + K * (Z - H*F*mu)
				S = (numpy.identity(6) - K*H)*(FSFTSX)
			# ---=== =============== ===--- #
                                distance = math.sqrt((tank.x - mu[0])**2 + (tank.y - mu[3])**2)
                                print "Distance %f" % distance
                                time = distance / self.bulletSpeed
                                time = math.floor(time / 0.5)
                                print time
                                i = 0
                                new_mu = mu
                                while i < time:
                                    new_mu = F * new_mu
                                    points.append(new_mu)
                                    i += 1
                                self.target = H * new_mu
                                print self.target
                                
                                distanceX = self.target[0] - tank.x
				distanceY = self.target[1] - tank.y
				target_angle = math.atan2(distanceY, distanceX)
				relative_angle = self.normalize_angle(target_angle - tank.angle)


				self.bzrc.angvel(tank.index,  0.75 * relative_angle)
                                if math.fabs(relative_angle) < 0.02:
                                    print "rel angle %f" % relative_angle
        			    self.bzrc.shoot(tank.index)
                                    

	def normalize_angle(self, angle):
		"""Make any angle be between +/- pi."""
		angle -= 2 * math.pi * int (angle / (2 * math.pi))
		if angle <= -math.pi:
			angle += 2 * math.pi
		elif angle > math.pi:
			angle -= 2 * math.pi
		return angle

def main():
        defineConsts()
	# Process CLI arguments.
	try:
		execname, host, port = sys.argv
	except ValueError:
		execname = sys.argv[0]
		print >>sys.stderr, '%s: incorrect number of arguments' % execname
		print >>sys.stderr, 'usage: %s hostname port' % sys.argv[0]
		sys.exit(-1)

	# Connect.
	#bzrc = BZRC(host, int(port), debug=True)
	bzrc = BZRC(host, int(port))

	agent = Agent(bzrc)

	prev_time = time.time()

	# Run the agent
	try:
		            ########################################################################
            # Plot the potential fields to a file
            print 'making file'
            outfile = open(FILENAME, 'w')
            print >>outfile, gnuplot_header(-WORLDSIZE / 2, WORLDSIZE / 2)
            #print >>outfile, draw_obstacles(OBSTACLES)
            field_function = generate_field_function(150)
            print >>outfile, plot_stuffs(field_function)


            ########################################################################
            # Animate a changing field, if the Python Gnuplot library is present

            try:
	            from Gnuplot import GnuplotProcess
            except ImportError:
	            print "Sorry.  You don't have the Gnuplot module installed."


            forward_list = list(linspace(ANIMATION_MIN, ANIMATION_MAX, ANIMATION_FRAMES/2))
            backward_list = list(linspace(ANIMATION_MAX, ANIMATION_MIN, ANIMATION_FRAMES/2))
            anim_points = forward_list + backward_list

            gp = GnuplotProcess(persist=False)
            gp.write(gnuplot_header(-WORLDSIZE / 2, WORLDSIZE / 2))
            #	gp.write(draw_obstacles(OBSTACLES))
            print "Before for loop"
            for scale in cycle(anim_points):
	            time_diff = time.time() - prev_time
	            if(time_diff < 0.5):
		            continue
	            prev_time = time.time()
	            agent.tick(time_diff)
                        
	            field_function = generate_field_function(scale)
	            gp.write(plot_stuffs(field_function))
            print "After for loop"

	except KeyboardInterrupt:
		print "Exiting due to keyboard interrupt."
		bzrc.close()

def defineConsts():
        #---=== Constant Matrices; Calculate once ===---#
        print "defining consts"
	global dt
	dt = 0.5

	global F
	F = numpy.matrix([[1, dt, dt*dt/2, 0, 0, 0],	#Physics equations
					  [0, 1, dt, 0, 0, 0],
					  [0, 0, 1, 0, 0, 0],
					  [0, 0, 0, 1, dt, dt*dt/2],
					  [0, 0, 0, 0, 1, dt],
					  [0, 0, 0, 0, 0, 1]])

	global Sx
	Sx = numpy.matrix([[0.1, 0, 0, 0, 0, 0],  #Uncertainty in physics
					   [0, 0.1, 0, 0, 0, 0],
					   [0, 0, 1, 0, 0, 0],
					   [0, 0, 0, 0.1, 0, 0],
					   [0, 0, 0, 0, 0.1, 0],
					   [0, 0, 0, 0, 0, 10]])				

	global H
	H = numpy.matrix([[1, 0, 0, 0, 0, 0],	#Pick out X and Y
					  [0, 0, 0, 1, 0, 0]])

	global Sz
	Sz = numpy.matrix([[25, 0],	 #Uncertainty in X, Y sensor
					   [0, 25]])
        #---=== =============================== === ---#

        #---=== To be updated each tick; But initialize some guesses ===---#
	global mu
	mu = numpy.matrix([[0], #x	  #Estimate of tank state
					   [0], #x'
					   [0], #x''
					   [0], #y
					   [0], #y'
					   [0]])#y''

	global S
	S = numpy.matrix([[100, 0, 0, 0, 0, 0], #Uncertainty in estimate
					  [0, 0.1, 0, 0, 0, 0],
					  [0, 0, 0.1, 0, 0, 0],
					  [0, 0, 0, 100, 0, 0],
					  [0, 0, 0, 0, 0.1, 0],
					  [0, 0, 0, 0, 0, 0.1]])
        #---=== =============================== === ---#

        global points
        points = []

if __name__ == '__main__':
	main()




# vim: et sw=4 sts=4



# vim: et sw=4 sts=4
