#!/usr/bin/python -tt

# An incredibly simple agent.  All we do is find the closest enemy tank, drive
# towards it, and shoot.  Note that if friendly fire is allowed, you will very
# often kill your own tanks with this code.

#################################################################
# NOTE TO STUDENTS
# This is a starting point for you.  You will need to greatly
# modify this code if you want to do anything useful.  But this
# should help you to know how to interact with BZRC in order to
# get the information you need.
#
# After starting the bzrflag server, this is one way to start
# this code:
# python agent0.py [hostname] [port]
#
# Often this translates to something like the following (with the
# port name being printed out by the bzrflag server):
# python agent0.py localhost 49857
#################################################################

import sys
import math
import time
import random

from bzrc import BZRC, Command
from random import randint

class Agent(object):
	"""Class handles all command and control logic for a teams tanks."""

	#10_DEGREES = 0.1745

	def __init__(self, bzrc):
		self.bzrc = bzrc
		self.constants = self.bzrc.get_constants()
		self.commands = []
		self.movement_timer = 0
		self.target_turn_time = 0
		self.needs_target_turn_time = True
		self.target_shoot_time = 0
		self.needs_target_shoot_time = True
		self.shoot_timer = 0
		self.target_flag = ['','','','','','','','','','']
		#self.obstacles = self.bzrc.get_obstacles()
		bases = self.bzrc.get_bases()
		for base in bases:
			if base.color == self.constants['team']:
				self.mybase = base
				break		

	def tick(self, time_diff):
		self.shoot_timer += time_diff
			
		"""Some time has passed; decide what to do next."""
		mytanks, othertanks, flags, shots = self.bzrc.get_lots_o_stuff()
		self.mytanks = mytanks
		self.othertanks = othertanks
		self.flags = flags
		self.shots = shots
		self.enemies = [tank for tank in othertanks if tank.color !=
						self.constants['team']]
		
		print "checking tanks"
		for tank in mytanks:
			if tank.status != 'alive':
				self.target_flag[tank.index] = ''
				continue

			flag_rad = 100
			enemy_rad = 100
			base_rad = 100
			obstacle_rad = 50
			Xdir = 0
			Ydir = 0
			target_flag = 'none'
			closest_dist = 12000
			closest_enemy_dist = 12000
			closest_friend_dist = 12000
			enemy_angle = 6
			friend_angle = 6

		#---=== Get repulsed from every enemy tank ===---#
			for enemy in self.enemies:				
				distanceX = enemy.x - tank.x
				distanceY = enemy.y - tank.y
				target_angle = math.atan2(distanceY, distanceX)
				relative_angle = self.normalize_angle(target_angle - tank.angle)
				tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)

				if (tot_distance < closest_enemy_dist and math.fabs(relative_angle) < 0.349) or tot_distance < 20:	#Only count if enemy is in front of you
					closest_enemy_dist = tot_distance

				if tot_distance > enemy_rad:	#If you are too far away, don't worry
					continue
				if enemy.flag == target_flag:   #If the enemy has the flag you are trying to get, don't run away
					continue
				if tank.flag != '-':            #If you have the flag just run
					continue

				if closest_dist < 100:	  #If you are very close to your target, stop squirming around and just get it
					break
				Xdir -= (distanceX / tot_distance) * (enemy_rad - tot_distance) / enemy_rad
				Ydir -= (distanceY / tot_distance) * (enemy_rad - tot_distance) / enemy_rad
				#print "xdir: %s" % Xdir

			for friend in mytanks:  #Find closest friend
				if tank.index != friend.index:
					distanceX = friend.x - tank.x
					distanceY = friend.y - tank.y
					target_angle = math.atan2(distanceY, distanceX)
					relative_angle = self.normalize_angle(target_angle - tank.angle)
					tot_distance = math.sqrt(distanceX ** 2 + distanceY ** 2)
					if tot_distance < closest_friend_dist and math.fabs(relative_angle) < 0.349:  #Only count if the friend is in front of you (within 20 degrees on either side)
						closest_friend_dist = tot_distance

		

			'''DO THE MOVE'''
			if Xdir > 4:
				print "FINAL xdir %s:" % Xdir

			target_angle = math.atan2(Ydir, Xdir)
			relative_angle = self.normalize_angle(target_angle - tank.angle)
			self.bzrc.angvel(tank.index, relative_angle)
			target_velocity = math.sqrt(Xdir ** 2 + Ydir ** 2)
			if target_velocity > 1:
				target_velocity = 1
			target_velocity *= (math.pi - 0.5 * math.fabs(relative_angle)) / math.pi #if you still need to turn, slow down a bit
			#print "velocity %s" % target_velocity
			self.bzrc.speed(tank.index, target_velocity)

			if closest_enemy_dist < closest_friend_dist:	#only shoot if an enemy is closer than a visible friend
				self.bzrc.angvel(tank.index, random.uniform(0,0.1))
			if math.fabs(tank.vx) < 0.1 and math.fabs(tank.vy) < 0.1:		   #or if you are stuck
				self.bzrc.angvel(tank.index, random.uniform(0,1))

	def normalize_angle(self, angle):
		"""Make any angle be between +/- pi."""
		angle -= 2 * math.pi * int (angle / (2 * math.pi))
		if angle <= -math.pi:
			angle += 2 * math.pi
		elif angle > math.pi:
			angle -= 2 * math.pi
		return angle

def main():
	# Process CLI arguments.
	try:
		execname, host, port = sys.argv
	except ValueError:
		execname = sys.argv[0]
		print >>sys.stderr, '%s: incorrect number of arguments' % execname
		print >>sys.stderr, 'usage: %s hostname port' % sys.argv[0]
		sys.exit(-1)

	# Connect.
	#bzrc = BZRC(host, int(port), debug=True)
	bzrc = BZRC(host, int(port))

	agent = Agent(bzrc)

	prev_time = time.time()

	# Run the agent
	try:
		while True:
			time_diff = time.time() - prev_time
			prev_time = time.time()
			agent.tick(time_diff)
	except KeyboardInterrupt:
		print "Exiting due to keyboard interrupt."
		bzrc.close()


if __name__ == '__main__':
	main()

# vim: et sw=4 sts=4
